import React from 'react';
import ReactDOM from 'react-dom';
import "../../styles/ProductsDetails.scss";
import BreadCrumbs from '../Assets/BreadCrumbs';
import NeedHelp from '../../components/Need-Help';
import { Tabs, Tab, Row, Col, Nav } from 'react-bootstrap';
import {
    Magnifier,
    GlassMagnifier,
    SideBySideMagnifier,
    PictureInPictureMagnifier
  } from "react-image-magnifiers";
 
const ProductsDetails = () => {

    return (
        <React.Fragment>
            <BreadCrumbs />
            <div className="single-product-wrapper">
                <section className="product-details">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-6">
                                <div className="product-image">
                                    <GlassMagnifier
                                   
                                        imageSrc={require('../../images/english-placeholder_1.png')}
                                        imageAlt="Product Image"
                                       
                                        />
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className="product-info product-info-main">
                                    <div className="page-title-wrapper product">
                                        <h1 className="page-title">
                                            <span className="base">Whole Chicken Griller 800g - Sadia </span>
                                        </h1>
                                    </div>
                                    <div className="review-container">
                                        <div className="product-reviews-summary short">
                                            <div className="rating-summary">
                                                <span className="label">
                                                    <span>Rating:</span></span>
                                                <div className="rating-result" title="93%">
                                                    <span style={{ width: "93%" }}><span>93%</span></span>
                                                </div>
                                            </div>
                                            <div className="reviews-actions">
                                                <a className="action view" href="#">4&nbsp;<span>Reviews</span></a><a className="action add" href="#">Add Your Review</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="product-info-price">
                                        <div className="product-reviews-summary empty">
                                            <div className="reviews-actions">
                                                <a className="action add" href="#">
                                                    Be the first to review this product
</a>
                                            </div>
                                        </div>
                                        <div className="price-box price-final_price">
                                            <span className="normal-price">
                                                <span className="price-container price-final_price tax weee">
                                                    <span className="price-label">As low as</span>
                                                    <span id="product-price-144" className="price-wrapper ">
                                                        <span className="price">KWD 6.750</span></span>
                                                    <span className="average-weight">
                                                    </span>
                                                </span>
                                            </span>
                                            <span className="average-weight">
                                                Average Weight 2.5 to 3.5 Kg
</span>
                                        </div>
                                        <div className="product_manufacture">
                                            <h3 className="country-of-manufacture"><strong>Origin - </strong> Kuwait</h3>
                                        </div>
                                        <div className="product attribute overview">
                                            Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC.
</div>
                                        <div className="product-add-form">
                                            <form action="#" method="post" id="product_addtocart_form">
                                                <div className="product-options-wrapper" id="product-options-wrapper" >
                                                    <div className="fieldset">
                                                        <div className="swatch-opt">
                                                            <div className="swatch-attribute product_in_piece">
                                                                <span id="option-label-product_in_piece-219" className="swatch-attribute-label">Pack Type</span>

                                                                <div className="swatch-attribute-options clearfix">
                                                                    <select className="swatch-option">
                                                                        <option value="5455">1 Piece KWD 0.680</option>
                                                                        <option value="5461">Carton(10 Pieces) KWD 6.750</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="product-options-bottom">
                                                    <div className="box-tocart">
                                                        <div className="fieldset">
                                                            <div className="field qty">
                                                                <div className="control">
                                                                    <button id="decerement"><span>-</span></button>
                                                                    <input type="number" name="qty" id="qty" min="0" value="1" title="Qty" className="product-qty input-text qty" />
                                                                    <button id="increment"><span>+</span></button>
                                                                </div>
                                                            </div>
                                                            <div className="actions">
                                                                <button type="submit" title="Add to Cart" className="action primary tocart" id="product-addtocart-button">
                                                                    <span>Add to Cart</span>
                                                                </button>

                                                            </div>
                                                            <div className="box-tocart-right">
                                                                <ul>
                                                                    <li className="wishlist"> <a href="#" className="action towishlist"></a></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="product-discription-tab">
                        <div className="container-fluid">
                            <Tab.Container id="left-tabs-example product-tab-list" defaultActiveKey="description">
                                <Row>
                                    <Col sm={3}>
                                        <Nav variant="pills" className="flex-column">
                                            <Nav.Item>
                                                <Nav.Link eventKey="description">Description</Nav.Link>
                                            </Nav.Item>
                                            <Nav.Item>
                                                <Nav.Link eventKey="information">Nutritional Information</Nav.Link>
                                            </Nav.Item>
                                            <Nav.Item>
                                                <Nav.Link eventKey="cooking">Cooking Method</Nav.Link>
                                            </Nav.Item>
                                        </Nav>
                                    </Col>
                                    <Col sm={9}>
                                        <Tab.Content>
                                            <Tab.Pane eventKey="description">
                                                <div className="discription-box">
                                                    <div className="disc-title">
                                                        <h3>Description</h3>
                                                    </div>
                                                    <div className="tab-content-sec">
                                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer laoreet orci erat, ac consectetur lectus dapibus non. Integer efficitur ultrices ultrices. Integer efficitur suscipit nulla nec scelerisque. Ut tincidunt vulputate scelerisque. Fusce semper velit eu ex tempus vulputate. Mauris magna lacus, porttitor eu aliquam at, venenatis vel neque. Etiam ut sapien elit.</p>
                                                        <p>Pellentesque et neque risus. Phasellus vitae velit maximus quam vestibulum hendrerit vitae eu leo. Quisque non purus diam. Suspendisse eget nibh magna. Pellentesque aliquet rutrum est ut feugiat. Phasellus lobortis justo ut sapien luctus scelerisque. Aenean sit amet mauris diam. In interdum sollicitudin convallis. Donec condimentum ut mi non euismod. Quisque gravida orci vitae sem sollicitudin, quis placerat nisi faucibus. Praesent vel lacinia lectus.</p>
                                                    </div>
                                                </div>
                                            </Tab.Pane>
                                            <Tab.Pane eventKey="information" className="Nutritional Information">
                                                <div className="discription-box">
                                                    <div className="disc-title">
                                                        <h3>Nutritional Information</h3>
                                                    </div>
                                                    <div className="tab-content-sec">
                                                        <div className="row">
                                                            <div className="col-md-6">
                                                                <div className="nutritional-calorie-diet">
                                                                    <span>*Percent Daily Values are based on a 2,000 calorie diet</span>
                                                                </div>
                                                                <div className="nutritional-circle-main">
                                                                    <div className="nutritional-circle-conatiner">
                                                                        <span>Calories</span>
                                                                        <div id="calories">
                                                                            <svg viewBox="0 0 100 100">
                                                                                <path d="M 50,50 m 0,-47 a 47,47 0 1 1 0,94 a 47,47 0 1 1 0,-94" stroke="#eee" stroke-width="6" fill-opacity="0"></path>
                                                                                <path d="M 50,50 m 0,-47 a 47,47 0 1 1 0,94 a 47,47 0 1 1 0,-94" stroke="rgb(150,189,169)" stroke-width="6" fill-opacity="0">20</path>
                                                                            </svg>
                                                                            <div className="progressbar-text" >20<br /><div className="dv-text">~1% DV*</div></div>
                                                                        </div>
                                                                    </div>
                                                                    <div className="nutritional-circle-conatiner">
                                                                        <span>Protein</span>
                                                                        <div id="protein">
                                                                            <svg viewBox="0 0 100 100">
                                                                                <path d="M 50,50 m 0,-47 a 47,47 0 1 1 0,94 a 47,47 0 1 1 0,-94" stroke="#eee" stroke-width="6" fill-opacity="0"></path>
                                                                                <path d="M 50,50 m 0,-47 a 47,47 0 1 1 0,94 a 47,47 0 1 1 0,-94" stroke="rgb(158,191,133)" stroke-width="6" fill-opacity="0"></path>
                                                                            </svg>
                                                                            <div className="progressbar-text">21<br />
                                                                                <div className="dv-text">~8% DV*</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div className="nutritional-circle-conatiner">
                                                                        <span>Sat. Fat</span>
                                                                        <div id="sat">
                                                                            <svg viewBox="0 0 100 100" >
                                                                                <path d="M 50,50 m 0,-47 a 47,47 0 1 1 0,94 a 47,47 0 1 1 0,-94" stroke="#eee" stroke-width="6" fill-opacity="0"></path>
                                                                                <path d="M 50,50 m 0,-47 a 47,47 0 1 1 0,94 a 47,47 0 1 1 0,-94" stroke="rgb(112,165,137)" stroke-width="6" fill-opacity="0" ></path>
                                                                            </svg>
                                                                            <div className="progressbar-text">22<br />
                                                                                <div className="dv-text">~10% DV*</div>
                                                                            </div>
                                                                        </div>

                                                                    </div>

                                                                </div>
                                                            </div>
                                                            <div className="col-md-6">
                                                                <table border="1"><tbody><tr><td>Iron</td>
                                                                    <td>10</td>


                                                                </tr><tr><td>Potassium</td>
                                                                        <td>12</td>


                                                                    </tr><tr><td>Riboflavin</td>
                                                                        <td>13</td>


                                                                    </tr><tr><td>Niacin</td>
                                                                        <td>14</td>


                                                                    </tr><tr><td>Vitamin B6</td>
                                                                        <td>15</td>


                                                                    </tr><tr><td>Vitamin B12</td>
                                                                        <td>16</td>


                                                                    </tr><tr><td>Phosphorus</td>
                                                                        <td>17</td>


                                                                    </tr><tr><td>Zinc</td>
                                                                        <td>18</td>


                                                                    </tr><tr><td>Selenium</td>
                                                                        <td>19</td>


                                                                    </tr></tbody></table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </Tab.Pane>
                                            <Tab.Pane eventKey="cooking">
                                                <div className="discription-box cooking-content">
                                                    <div className="disc-title">
                                                        <h3>Cooking Method</h3>
                                                    </div>
                                                    <div className="tab-content-sec">
                                                        <div className="cooking-method">
                                                            <span>This fine peace of meat can be cooked using a variety of methods.</span>
                                                            <ul>
                                                                <li>
                                                                    <a href="#">
                                                                        <div className="image">
                                                                            <p>
                                                                                <img src={require('../../images/grilling-new.png')} alt="grilling" />
                                                                            </p>
                                                                        </div>
                                                                        <h4>Grilling</h4>
                                                                    </a>
                                                                </li>



                                                                <li>
                                                                    <a href="#">
                                                                        <div className="image">
                                                                            <p><img src={require('../../images/roast.png')} alt="roast" /></p>
                                                                        </div>
                                                                        <h4>Roast Or Bake</h4>
                                                                    </a>
                                                                </li>



                                                                <li>
                                                                    <a href="#">
                                                                        <div className="image">
                                                                            <p><img className="pan" src={require('../../images/pan-broil.png')} alt="pan-broil" />
                                                                            </p>
                                                                        </div>
                                                                        <h4>Pan Broil / Skillet</h4>
                                                                    </a>
                                                                </li>



                                                                <li>
                                                                    <a href="#">
                                                                        <div className="image">
                                                                            <p><img className="pan" src={require('../../images/pot-roast.png')} alt="pot-roast" /></p>
                                                                        </div>
                                                                        <h4>Pot Roast / Braise</h4>
                                                                    </a>
                                                                </li>

                                                                <li>
                                                                    <a href="#">
                                                                        <div className="image">
                                                                            <p><img className="pan" src={require('../../images/saous.png')} alt="saous" /></p>
                                                                        </div>
                                                                        <h4>Sous Vide</h4>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </Tab.Pane>
                                        </Tab.Content>
                                    </Col>
                                </Row>
                            </Tab.Container>
                        </div>
                    </div>
                    <div className="product-full-width-section product-reviews-container">
                        <div className="container">
                            <h2 className="product-section-title">Reviews</h2>
                            <div className="block review-list" id="customer-reviews">
                                <div className="block-content">
                                    <div className="toolbar review-toolbar">
                                    </div>
                                    <ol className="items review-items">
                                        <li className="item review-item">
                                            <div className="review-title">test111</div>
                                            <div className="review-ratings">
                                                <div className="rating-summary item">
                                                    <span className="label rating-label"><span>Ratings</span></span>
                                                    <div className="rating-result" title="100%">
                                                        <span style={{ width: "100%" }}>
                                                            <span>100%</span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="review-content">
                                                test
                                                 </div>
                                            <div className="review-details">
                                                <p className="review-author">
                                                    <span className="review-details-label">Review by</span>
                                                    <strong className="review-details-value">izhar</strong>
                                                </p>
                                                <p className="review-date">
                                                    <span className="review-details-label">Posted on</span>
                                                    <time className="review-details-value" >10/1/20</time>
                                                </p>
                                            </div>
                                        </li>

                                        <li className="item review-item">
                                            <div className="review-title">test111</div>
                                            <div className="review-ratings">
                                                <div className="rating-summary item">
                                                    <span className="label rating-label"><span>Ratings</span></span>
                                                    <div className="rating-result" title="100%">
                                                        <span style={{ width: "100%" }}>
                                                            <span>100%</span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="review-content">
                                                test
                                                 </div>
                                            <div className="review-details">
                                                <p className="review-author">
                                                    <span className="review-details-label">Review by</span>
                                                    <strong className="review-details-value">izhar</strong>
                                                </p>
                                                <p className="review-date">
                                                    <span className="review-details-label">Posted on</span>
                                                    <time className="review-details-value" >10/1/20</time>
                                                </p>
                                            </div>
                                        </li>

                                    </ol>

                                </div>
                            </div>

                            <div className="block review-add">
                                <div className="container-full">
                                    <div className="block-title"><strong>Write Your Own Review</strong></div>
                                    <div className="block-content">
                                        <form action="#" className="review-form" method="post" id="review-form">
                                            <fieldset className="fieldset review-fieldset">
                                                <legend className="legend review-legend"><span>You're reviewing:</span><strong>Test Price Per Kg</strong></legend>
                                                <span id="input-message-box"></span>
                                                <fieldset className="field required review-field-ratings">
                                                    <legend className="label"><span>Your Rating</span></legend>
                                                    <div className="control">
                                                        <div className="nested" id="product-review-table">
                                                            <div className="field choice review-field-rating">
                                                                <label className="label" id="Ratings_rating_label"><span>Ratings</span></label>
                                                                <div className="control review-control-vote">
                                                                    <input type="radio" name="ratings[4]" id="Ratings_1" value="16" className="radio" />
                                                                    <label className="rating-1" for="Ratings_1" title="1 star" id="Ratings_1_label">
                                                                        <span>1 star</span>
                                                                    </label>
                                                                    <input type="radio" name="ratings[4]" id="Ratings_2" value="17" className="radio" />
                                                                    <label className="rating-2" for="Ratings_2" title="2 stars" id="Ratings_2_label">
                                                                        <span>2 stars</span>
                                                                    </label>
                                                                    <input type="radio" name="ratings[4]" id="Ratings_3" value="18" className="radio" />
                                                                    <label className="rating-3" for="Ratings_3" title="3 stars" id="Ratings_3_label">
                                                                        <span>3 stars</span>
                                                                    </label>
                                                                    <input type="radio" name="ratings[4]" id="Ratings_4" value="19" className="radio" />
                                                                    <label className="rating-4" for="Ratings_4" title="4 stars" id="Ratings_4_label">
                                                                        <span>4 stars</span>
                                                                    </label>
                                                                    <input type="radio" name="ratings[4]" id="Ratings_5" value="20" className="radio" />
                                                                    <label className="rating-5" for="Ratings_5" title="5 stars" id="Ratings_5_label">
                                                                        <span>5 stars</span>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </fieldset>
                                                <div className="field review-field-nickname required">
                                                    <label for="nickname_field" className="label"><span>Nickname</span></label>
                                                    <div className="control">
                                                        <input type="text" name="nickname" id="nickname_field" className="input-text" />
                                                    </div>
                                                </div>
                                                <div className="field review-field-summary required">
                                                    <label for="summary_field" className="label"><span>Summary</span></label>
                                                    <div className="control">
                                                        <input type="text" name="title" id="summary_field" className="input-text" />
                                                    </div>
                                                </div>
                                                <div className="field review-field-text required">
                                                    <label for="review_field" className="label"><span>Review</span></label>
                                                    <div className="control">
                                                        <textarea name="detail" id="review_field" cols="5" rows="3"></textarea>
                                                    </div>
                                                </div>
                                            </fieldset>
                                            <div className="actions-toolbar review-form-actions">
                                                <div className="primary actions-primary">
                                                    <button type="submit" className="action submit primary"><span>Submit Review</span></button>
                                                </div>
                                            </div>
                                        </form>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <div className="product-display related-products">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-12">
                                <div class="block-title title">
                                    <h2 id="block-related-heading">You May Also Like</h2>
                                    <span>Related Products others have bought with beef Sirloin</span>
                                </div>
                            </div>
                        </div>
                        <div className="row gallery-images related-products">
                            <div className="col-sm-3 gallery-img-holder">
                                <div className="product-wrapper">

                                    <div className="product-wrapper-container">
                                        <a href="#">
                                            <img className="product-img" src={require('../../images/english-placeholder_1.png')} alt="" />
                                        </a>
                                        <div className="product-hover">
                                            <ul>
                                                <li className="wishlist">
                                                    <a href="#" className="action towishlist">
                                                    </a>
                                                </li>

                                                <li className="add-to-cart">
                                                    <div className="actions add-to-cart">
                                                        <button type="submit" title="Add to Bag" className="action tocart primary">
                                                            <span>Add to Bag</span>
                                                        </button>
                                                        <div className="product-item-inner">
                                                            <button type="submit" title="Add to Bag" className="action tocart primary"></button>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>

                                        </div>

                                    </div>
                                    <div className="product-desc">
                                        <h4 className="product-name">
                                            <a href="#">Beef Breakfast Slices - Chefkom</a>
                                        </h4>
                                        <div className="product-price">
                                            <div className="price-box price-final_price">
                                                <span className="price-container price-final_price tax weee">
                                                    <span id="product-price-289" className="price-wrapper ">
                                                        <span className="price">KWD 5.700</span>
                                                    </span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div className="col-sm-3 gallery-img-holder">
                                <div className="product-wrapper">

                                    <div className="product-wrapper-container">
                                        <a href="#">
                                            <img className="product-img" src={require('../../images/english-placeholder_1.png')} alt="" />
                                        </a>
                                        <div className="product-hover">
                                            <ul>
                                                <li className="wishlist">
                                                    <a href="#" className="action towishlist">
                                                    </a>
                                                </li>

                                                <li className="add-to-cart">
                                                    <div className="actions add-to-cart">
                                                        <button type="submit" title="Add to Bag" className="action tocart primary">
                                                            <span>Add to Bag</span>
                                                        </button>
                                                        <div className="product-item-inner">
                                                            <button type="submit" title="Add to Bag" className="action tocart primary"></button>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>

                                        </div>

                                    </div>
                                    <div className="product-desc">
                                        <h4 className="product-name">
                                            <a href="#">Beef Breakfast Slices - Chefkom</a>
                                        </h4>
                                        <div className="product-price">
                                            <div className="price-box price-final_price">
                                                <span className="price-container price-final_price tax weee">
                                                    <span id="product-price-289" className="price-wrapper ">
                                                        <span className="price">KWD 5.700</span>
                                                    </span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div className="col-sm-3 gallery-img-holder">
                                <div className="product-wrapper">

                                    <div className="product-wrapper-container">
                                        <a href="#">
                                            <img className="product-img" src={require('../../images/english-placeholder_1.png')} alt="" />
                                        </a>
                                        <div className="product-hover">
                                            <ul>
                                                <li className="wishlist">
                                                    <a href="#" className="action towishlist">
                                                    </a>
                                                </li>

                                                <li className="add-to-cart">
                                                    <div className="actions add-to-cart">
                                                        <button type="submit" title="Add to Bag" className="action tocart primary">
                                                            <span>Add to Bag</span>
                                                        </button>
                                                        <div className="product-item-inner">
                                                            <button type="submit" title="Add to Bag" className="action tocart primary"></button>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>

                                        </div>

                                    </div>
                                    <div className="product-desc">
                                        <h4 className="product-name">
                                            <a href="#">Beef Breakfast Slices - Chefkom</a>
                                        </h4>
                                        <div className="product-price">
                                            <div className="price-box price-final_price">
                                                <span className="price-container price-final_price tax weee">
                                                    <span id="product-price-289" className="price-wrapper ">
                                                        <span className="price">KWD 5.700</span>
                                                    </span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div className="col-sm-3 gallery-img-holder">
                                <div className="product-wrapper">

                                    <div className="product-wrapper-container">
                                        <a href="#">
                                            <img className="product-img" src="https://dev.makanifoods.com/media/catalog/product/cache/02db20edfbe38a05819d9dfb7c772bfa/b/e/beef_breakfast_slices_1_1.jpg" alt="" />
                                        </a>
                                        <div className="product-hover">
                                            <ul>
                                                <li className="wishlist">
                                                    <a href="#" className="action towishlist">
                                                    </a>
                                                </li>

                                                <li className="add-to-cart">
                                                    <div className="actions add-to-cart">
                                                        <button type="submit" title="Add to Bag" className="action tocart primary">
                                                            <span>Add to Bag</span>
                                                        </button>
                                                        <div className="product-item-inner">
                                                            <button type="submit" title="Add to Bag" className="action tocart primary"></button>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>

                                        </div>

                                    </div>
                                    <div className="product-desc">
                                        <h4 className="product-name">
                                            <a href="#">Beef Breakfast Slices - Chefkom</a>
                                        </h4>
                                        <div className="product-price">
                                            <div className="price-box price-final_price">
                                                <span className="price-container price-final_price tax weee">
                                                    <span id="product-price-289" className="price-wrapper ">
                                                        <span className="price">KWD 5.700</span>
                                                    </span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div className="product-display upsell upsell-products">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-12">
                                <div class="block-title title">
                                    <h2 id="block-related-heading">You May Also Like</h2>
                                    <span>Upsell Products others have bought with beef Sirloin</span>
                                </div>
                            </div>
                        </div>
                        <div className="row gallery-images upsell-products">
                            <div className="col-sm-3 gallery-img-holder">
                                <div className="product-wrapper">

                                    <div className="product-wrapper-container">
                                        <a href="#">
                                            <img className="product-img" src={require('../../images/english-placeholder_1.png')} alt="" />
                                        </a>
                                        <div className="product-hover">
                                            <ul>
                                                <li className="wishlist">
                                                    <a href="#" className="action towishlist">
                                                    </a>
                                                </li>

                                                <li className="add-to-cart">
                                                    <div className="actions add-to-cart">
                                                        <button type="submit" title="Add to Bag" className="action tocart primary">
                                                            <span>Add to Bag</span>
                                                        </button>
                                                        <div className="product-item-inner">
                                                            <button type="submit" title="Add to Bag" className="action tocart primary"></button>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>

                                        </div>

                                    </div>
                                    <div className="product-desc">
                                        <h4 className="product-name">
                                            <a href="#">Beef Breakfast Slices - Chefkom</a>
                                        </h4>
                                        <div className="product-price">
                                            <div className="price-box price-final_price">
                                                <span className="price-container price-final_price tax weee">
                                                    <span id="product-price-289" className="price-wrapper ">
                                                        <span className="price">KWD 5.700</span>
                                                    </span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div className="col-sm-3 gallery-img-holder">
                                <div className="product-wrapper">

                                    <div className="product-wrapper-container">
                                        <a href="#">
                                            <img className="product-img" src={require('../../images/english-placeholder_1.png')} alt="" />
                                        </a>
                                        <div className="product-hover">
                                            <ul>
                                                <li className="wishlist">
                                                    <a href="#" className="action towishlist">
                                                    </a>
                                                </li>

                                                <li className="add-to-cart">
                                                    <div className="actions add-to-cart">
                                                        <button type="submit" title="Add to Bag" className="action tocart primary">
                                                            <span>Add to Bag</span>
                                                        </button>
                                                        <div className="product-item-inner">
                                                            <button type="submit" title="Add to Bag" className="action tocart primary"></button>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>

                                        </div>

                                    </div>
                                    <div className="product-desc">
                                        <h4 className="product-name">
                                            <a href="#">Beef Breakfast Slices - Chefkom</a>
                                        </h4>
                                        <div className="product-price">
                                            <div className="price-box price-final_price">
                                                <span className="price-container price-final_price tax weee">
                                                    <span id="product-price-289" className="price-wrapper ">
                                                        <span className="price">KWD 5.700</span>
                                                    </span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div className="col-sm-3 gallery-img-holder">
                                <div className="product-wrapper">

                                    <div className="product-wrapper-container">
                                        <a href="#">
                                            <img className="product-img" src={require('../../images/english-placeholder_1.png')} alt="" />
                                        </a>
                                        <div className="product-hover">
                                            <ul>
                                                <li className="wishlist">
                                                    <a href="#" className="action towishlist">
                                                    </a>
                                                </li>

                                                <li className="add-to-cart">
                                                    <div className="actions add-to-cart">
                                                        <button type="submit" title="Add to Bag" className="action tocart primary">
                                                            <span>Add to Bag</span>
                                                        </button>
                                                        <div className="product-item-inner">
                                                            <button type="submit" title="Add to Bag" className="action tocart primary"></button>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>

                                        </div>

                                    </div>
                                    <div className="product-desc">
                                        <h4 className="product-name">
                                            <a href="#">Beef Breakfast Slices - Chefkom</a>
                                        </h4>
                                        <div className="product-price">
                                            <div className="price-box price-final_price">
                                                <span className="price-container price-final_price tax weee">
                                                    <span id="product-price-289" className="price-wrapper ">
                                                        <span className="price">KWD 5.700</span>
                                                    </span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div className="col-sm-3 gallery-img-holder">
                                <div className="product-wrapper">

                                    <div className="product-wrapper-container">
                                        <a href="#">
                                            <img className="product-img" src="https://dev.makanifoods.com/media/catalog/product/cache/02db20edfbe38a05819d9dfb7c772bfa/b/e/beef_breakfast_slices_1_1.jpg" alt="" />
                                        </a>
                                        <div className="product-hover">
                                            <ul>
                                                <li className="wishlist">
                                                    <a href="#" className="action towishlist">
                                                    </a>
                                                </li>

                                                <li className="add-to-cart">
                                                    <div className="actions add-to-cart">
                                                        <button type="submit" title="Add to Bag" className="action tocart primary">
                                                            <span>Add to Bag</span>
                                                        </button>
                                                        <div className="product-item-inner">
                                                            <button type="submit" title="Add to Bag" className="action tocart primary"></button>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>

                                        </div>

                                    </div>
                                    <div className="product-desc">
                                        <h4 className="product-name">
                                            <a href="#">Beef Breakfast Slices - Chefkom</a>
                                        </h4>
                                        <div className="product-price">
                                            <div className="price-box price-final_price">
                                                <span className="price-container price-final_price tax weee">
                                                    <span id="product-price-289" className="price-wrapper ">
                                                        <span className="price">KWD 5.700</span>
                                                    </span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <NeedHelp />
        </React.Fragment >
    );
}

export default ProductsDetails;

