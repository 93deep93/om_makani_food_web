import React from 'react';
import ReactDOM from 'react-dom';
import "../../styles/BredCrumbs.scss";
const BreadCrumbs = () => {

    return (
        <React.Fragment>
            <div class="breadcrumbs">
                <div className="container">
                <ul class="items">
                    <li class="item home">
                        <a href="https://dev.makanifoods.com/" title="Go to Home Page">
                            Home               
                         </a>
                    </li>
                    <li class="item category3">
                        <strong>Shop</strong>
                    </li>
                </ul>
                </div>
            </div>
        </React.Fragment>
    );
}

export default BreadCrumbs;