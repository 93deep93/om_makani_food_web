import React from 'react';
import ReactDOM from 'react-dom';

import NeedHelp from '../components/Need-Help';
import BreadCrumbs from '../components/BreadCrumbs';
const HowitWorks = () => {

    return (
        <React.Fragment>
            <BreadCrumbs />
            <div className="showroom-lcation">
                <div className="page-title-wrapper container">
                    <h1 className="page-title" id="page-title-heading">
                        <span className="base">Showroom Locations</span>
                    </h1>

                </div>
                <section className="Showroom-location">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-12">
                                <div className="map">
                                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3478.575600607554!2d47.92533111434273!3d29.32412235919518!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3fcf9b51519aee8f%3A0xc57025e4f4e91b9!2sMAKANI%20-%20Alyasra%20Foods%20Co!5e0!3m2!1sen!2sus!4v1603960239293!5m2!1sen!2sus" width="100%" height="450" frameborder="0" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-12">
                                <div className="address-title">
                                    <h2>Contact Information</h2>
                                    <span>Delicious Food – Easily Made</span>
                                </div>
                            </div>
                            <div className="col-md-4">
                                <div className="store-item">
                                    <div className="image-full">
                                        <img className="shop" src={require('../images/Makani_Store_1.jpg')} alt="Shop" />
                                    </div>
                                    <div className="store-desc">
                                        <h4 className="store-name">Makani Foods Shuwaikh</h4>
                                        <div className="street-name">Shuwaikh Industrial</div>
                                        <div className="zip-code">Behind the old fruit market</div>
                                        <div className="driving-direction"><a href="https://goo.gl/maps/hv7prDSs1qv8NzF57">Driving directions</a></div>
                                        <div className="telephone"><a href="tel:+201809090">Tel 1809090</a></div>
                                        <div className="mail-id"><a href="mailto:storemail@makanifoods.com">storemail@makanifoods.com</a></div>
                                    </div>
                                </div>
                            </div>

                            <div className="col-md-4">
                                <div className="store-item">
                                    <div className="image-full">
                                        <img className="shop" src={require('../images/jahra_M.jpg')} alt="Shop" />
                                    </div>
                                    <div className="store-desc">
                                        <h4 className="store-name">Makani Foods Jahra</h4>
                                        <div className="street-name">Ain Jalute Street</div>
                                        <div className="zip-code">Behind the old fruit market</div>
                                        <div className="driving-direction"><a href="https://goo.gl/maps/FguNsofJ2V7VWkf27">Driving directions</a></div>
                                        <div className="telephone"><a href="tel:+201809090">Tel 1809090</a></div>
                                        <div className="mail-id"><a href="mailto:storemail@makanifoods.com">storemail@makanifoods.com</a></div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </section>
                <div className="neddhelp">
                    <NeedHelp />
                </div>
            </div>
        </React.Fragment>
    );
}

export default HowitWorks;

