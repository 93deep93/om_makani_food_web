import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../styles/MobileHeader.scss';
class MobileHeader extends React.Component{

    constructor(props) {
        super(props);
        this.state = {addClass: false}
      }
      toggle() {
        this.setState({addClass: !this.state.addClass});
      }
      
      render() {
        let boxClass = ["sections nav-sections"];
        let searchClass = ["control mobilesearch"];
        if(this.state.addClass) {
          boxClass.push('green');
        }
        else if (this.state.addClass) {
            searchClass.push('showform');
          }
        
    return (
        <React.Fragment>

            <header className="page-header mobil-header">
                <div className="header content header-container">
                    <span className="action nav-toggle" onClick={this.toggle.bind(this)}>
                        <span>Toggle Nav</span>
                    </span>
                    <a className="logo english" href="#" title="Makani Foods" aria-label="store logo">
                        <img src={require('../images/logo-english.svg')} title="Makani Foods" alt="Makani Foods" width="189" height="75" />
                    </a>
                    <a className="logo arabic" href="#" title="Makani Foods" aria-label="store logo">
                        <img src={require('../images/logo-arabic.svg')} title="Makani Foods" alt="Makani Foods" width="189" height="75" />
                    </a>
                    
                    <div className={boxClass.join(' ')}>
                        <ul className="header links">
                            <div className="menu-wrapper">
                            <div className="close-icon top-menu" onClick={this.toggle.bind(this)}><i className="fa fa-times" aria-hidden="true"></i></div>
                            <div className="login-wrapper-conatiner top-menu">
                                <a href="#">
                                    Login
	                            </a>
                            </div>
                            <span className="or">&nbsp;or&nbsp;</span>
                            <div className="signup-wrapper-container top-menu">
                                <a href="#">Sign Up</a>
                            </div>
                        </div>
                            <div className="login_overlay_bg"></div>

                            <li className="greet welcome">

                                <span className="not-logged-in" data-bind="html:&quot;&quot;"></span>

                            </li>

                        </ul>  <div className="nav-sections-item-content">
                            <nav className="navigation">
                                <div id="mySidenav" className="nav">
                                    <div className="nav-main">
                                        <ul className="">
                                            <li className="level0">

                                                <a className="level-top" href="#">Shop</a>
                                                <div className="menu-left">
                                                    <ul className="menu-links">
                                                        <li className="menu-parent active">
                                                            <a href="#">Products</a>
                                                            <span className="parent plus-icon">
                                                                <i className="fa fa-plus" aria-hidden="true"></i>
                                                            </span>
                                                            <div className="menu-right">
                                                                <ul className="menu-links-right">
                                                                    <li className="first-level">
                                                                        <ul>
                                                                            <li className="level1">
                                                                                <a href="#">Chicken &amp; Poultry</a>
                                                                                <span className="plus-icon"><i className="fa fa-plus" aria-hidden="true"></i></span>
                                                                                <ul className="level3">
                                                                                    <li className="third-level">
                                                                                        <a href="#">Whole Chickens</a>
                                                                                    </li>
                                                                                    <li className="third-level">
                                                                                        <a href="#">Breast</a>
                                                                                    </li>
                                                                                    <li className="third-level">
                                                                                        <a href="#">Legs &amp; Thighs</a>
                                                                                    </li>
                                                                                    <li className="third-level">
                                                                                        <a href="#">Wings &amp; Drumsticks</a>
                                                                                    </li>
                                                                                    <li className="third-level">
                                                                                        <a href="#">Livers &amp; Gizzards</a>
                                                                                    </li>
                                                                                    <li className="third-level">
                                                                                        <a href="#">Ready to Cook</a>
                                                                                    </li>

                                                                                </ul>
                                                                            </li>
                                                                            <li className="level1">
                                                                                <a href="#">Beef &amp; Veal</a>
                                                                                <span className="plus-icon"><i className="fa fa-plus" aria-hidden="true"></i></span>
                                                                                <ul className="level3">
                                                                                    <li className="third-level">
                                                                                        <a href="#">Steaks</a>
                                                                                    </li>
                                                                                    <li className="third-level">
                                                                                        <a href="#">Cubes &amp; Strips</a>
                                                                                    </li>
                                                                                    <li className="third-level">
                                                                                        <a href="#">Gounds &amp; Burgers</a>
                                                                                    </li>
                                                                                    <li className="third-level">
                                                                                        <a href="#">Ready to Cook</a>
                                                                                    </li>
                                                                                    <li className="third-level">
                                                                                        <a href="#">Ribs &amp; Roasts</a>
                                                                                    </li>

                                                                                </ul>
                                                                            </li>
                                                                            <li className="level1">
                                                                                <a href="#">Seafood</a>
                                                                                <span className="plus-icon"><i className="fa fa-plus" aria-hidden="true"></i></span>
                                                                                <ul className="level3">
                                                                                    <li className="third-level">
                                                                                        <a href="#">Fillets &amp; Portions</a>
                                                                                    </li>
                                                                                    <li className="third-level">
                                                                                        <a href="#">Shellfish</a>
                                                                                    </li>
                                                                                    <li className="third-level">
                                                                                        <a href="#">Shrimp</a>
                                                                                    </li>
                                                                                    <li className="third-level">
                                                                                        <a href="#">Ready to Cook</a>
                                                                                    </li>

                                                                                </ul>
                                                                            </li>
                                                                        </ul>                                                                                                                      <ul>                                        <li className="level1">
                                                                            <a href="#">Sides, Fruits &amp; Vegetables</a>
                                                                            <span className="plus-icon"><i className="fa fa-plus" aria-hidden="true"></i></span>
                                                                            <ul className="level3">
                                                                                <li className="third-level">
                                                                                    <a href="#">Fries &amp; Potato</a>
                                                                                </li>
                                                                                <li className="third-level">
                                                                                    <a href="#">Appetizers</a>
                                                                                </li>
                                                                                <li className="third-level">
                                                                                    <a href="#">Vegetables</a>
                                                                                </li>
                                                                                <li className="third-level">
                                                                                    <a href="#">Fruits</a>
                                                                                </li>

                                                                            </ul>
                                                                        </li>
                                                                            <li className="level1">
                                                                                <a href="#">Plant-based Alternatives</a>
                                                                                <span className="plus-icon"><i className="fa fa-plus" aria-hidden="true"></i></span>
                                                                                <ul className="level3">
                                                                                    <li className="third-level">
                                                                                        <a href="#">MorningStar Farms®</a>
                                                                                    </li>
                                                                                    <li className="third-level">
                                                                                        <a href="#">Beyond Meat®</a>
                                                                                    </li>

                                                                                </ul>
                                                                            </li>
                                                                            <li className="level1">
                                                                                <a href="#">Bread &amp; Desserts</a>
                                                                                <span className="plus-icon"><i className="fa fa-plus" aria-hidden="true"></i></span>
                                                                                <ul className="level3">
                                                                                    <li className="third-level">
                                                                                        <a href="#">Bread &amp; Buns</a>
                                                                                    </li>
                                                                                    <li className="third-level">
                                                                                        <a href="#">Pies, Cakes &amp; Cheesecakes</a>
                                                                                    </li>

                                                                                </ul>
                                                                            </li>
                                                                        </ul>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </li>

                                                        <li className="menu-parent">
                                                            <a href="#">Collections</a>
                                                            <span className="parent plus-icon"><i className="fa fa-plus" aria-hidden="true"></i></span>
                                                            <div className="menu-right">
                                                                <ul className="menu-links-right">
                                                                    <li className="first-level">
                                                                        <ul>
                                                                            <li className="level1">
                                                                                <a href="#">Organic</a>
                                                                            </li>


                                                                        </ul>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </li>


                                                    </ul>
                                                </div>
                                            </li>
                                            <li className="level0">

                                                <a className="level-top" href="#">How It works</a>
                                            </li>
                                            <li className="level0">

                                                <a className="level-top" href="#">Showroom Locations</a>
                                            </li>
                                            <li className="level0">
                                                <a className="level-top" href="#">Recipes</a>
                                            </li>
                                            <li className="level0">
                                                <a className="level-top" href="#">How-to</a>
                                            </li>
                                        </ul>

                                    </div>
                                </div>

                            </nav>
                        </div>

                        <div className="my-account">
                            <ul className="profile list-item">
                                <li><a href="#">My Profile</a></li>
                            </ul>
                            <ul className="my-order list-item">
                                <li><a href="#">My Orders</a></li>
                            </ul>
                            <ul className="support list-item">
                                <li><a href="#">Support &amp; Contact</a></li>
                            </ul>
                            <ul className="support list-item">
                                <li><a href="#">Faq</a></li>
                            </ul>
                        </div>
                        <div className="switcher store switcher-store" id="switcher-store">
                            <strong className="label switcher-label"><span>Select Store</span></strong>
                            <div className="actions dropdown options switcher-options">
                                <li className="switcher-option">
                                    <a href="javascript:void(0)">
                                        اللغة العربية
                                    </a>
                                </li>
                            </div>
                        </div>
                    </div>
                    <div className="right-container-wrap">
                        <div className="minicart-wrapper">
                            <a className="action showcart" href="#">
                                <span className="text">My Cart</span>
                                <span className="counter qty empty">
                                    <span className="counter-number"></span>
                                    <span className="counter-label">
                                    </span>
                                </span>
                            </a>

                            <div className="ui-dialog ui-widget ui-widget-content ui-corner-all ui-front mage-dropdown-dialog">
                                <div className="block block-minicart ui-dialog-content ui-widget-content">
                                    <div id="minicart-content-wrapper">

                                        <div className="block-title">
                                            <strong>
                                                <span className="text">My Cart</span>
                                                <span className="qty empty" title="Items in Cart">0</span>
                                            </strong>
                                        </div>

                                        <div className="block-content">
                                            <button type="button" id="btn-minicart-close" className="action close">
                                                <span>Close</span>
                                            </button>
                                            <strong className="subtitle empty">You have no items in your shopping cart.</strong>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div className="block block-search">
                            <div className="block block-title"><strong>Search and you shall find</strong></div>
                            <div className="block block-content">
                                <form className="form minisearch" id="search_mini_form" action="#" method="get">
                                    <div className="field search">
                                        <label className="label search-bnt" htmlFor="search" onClick={this.toggle.bind(this)}> 
                                            <span>Search</span>
                                        </label>
                                        <div  id="mobile-hide" className={searchClass.join(' ')}>
                                            <input type="text" name="q" value="" placeholder="Search" className="input-text" maxLength="128" />
                                            <span className="close-search">
                                                <i className="fa fa-times" aria-hidden="true"></i>
                                            </span>
                                            <div id="search_autocomplete" className="search-autocomplete"></div>
                                            <div className="actions">
                                                <button type="submit" title="Search" className="action search" disabled="">
                                                    <span>Search</span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div className="login-overlay">
                        <div className="login-popup">
                            <div className="response-msg"></div>
                            <div className="login-container">
                                <a href="#">Create account</a>
                                <p>Welcome Back :)</p>
                                <form className="popup-login-form" method="POST" action="">
                                    <fieldset className="fieldset">
                                        <div className="container field required">
                                            <input type="text" placeholder="E-mail or phone no." name="username" autoComplete="off" required="" />
                                            <input type="password" placeholder="Password" name="password" autoComplete="off" required="" />
                                            <div className="forgot">
                                                <a href="#">Forgot Your password ?</a>
                                            </div>
                                            <button className="action login primary" type="submit">Sign In</button>
                                        </div>
                                    </fieldset>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </header>

            <div className="delivery-container">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="free-delivery-text-main">
                                <div className="free-delivery-textmain">
                                    <p className="free-delivery-text">The minimum order for delivery is KD 10 - all displayed products are frozen for maximized freshness
                                        <img src={require('../images/frozen.mst.webp')} alt="frozen" />
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </React.Fragment>
    );
}
}

export default MobileHeader;