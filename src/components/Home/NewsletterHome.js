import React from 'react';
import ReactDOM from 'react-dom';
import '../../styles/NewsletterHome.scss';

const NewsletterHome = () => {

    return (
        <React.Fragment>
            <section className="newsletter-sec">
                <div className="container">
                    <div className="row">
                        <div className="col-md-6">
                            <div className="newsletter-explore">
                                <div className="title">
                                    <div className="best-deals-title">Get the Best Deals</div>
                                    <div className="subtext">Subscribe and receive our best offers in your inbox.</div>
                                </div>
                               
                                <div className="content">
                                    <form className="form subscribe" action="#" method="post">
                                        <div className="field newsletter">
                                            <div className="control">
                                                <label htmlFor="newsletter">
                                                    <span className="label">
                                                        Sign Up for Our Newsletter:
                                                    </span>
                                                    <input name="email" type="email" id="newsletter" placeholder="Enter your e-mail address" />
                                                </label>
                                            </div>
                                        </div>
                                        <div className="actions">
                                            <button className="action subscribe primary" title="Subscribe" type="submit">
                                                <span>Send</span>
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-6">
                            <div className="testimonials-explore">
                                <div className="best-deals-right">
                                    <div className="commas"></div>
                                    <p>The quality of the products I bought was very good. The delivery was excellent!".</p>
                                    <div className="author-name">Zahraa K, Jabriya</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>


        </React.Fragment>
    );
}

export default NewsletterHome;