import React from 'react';
import ReactDOM from 'react-dom';
import "../styles/NeedHelp.scss";

const NeedHelp = () => {

    return (
        <React.Fragment>
            <div className="after-shop">
                <div className="container">
                    <div className="row">
                        <div className="col-md-6">
                            <div className="images">
                                <img className="men" src={require('../images/help-women.svg')} alt="Women" />
                                <img className="women" src={require('../images/help-men.svg')} alt="Men" />
                            </div>
                        </div>
                        <div className="col-md-6">
                            <div className="help-section">
                                <h2>Need help?</h2>
                                <h3>If you have any questions or need assistance, we’d be more than happy to help.</h3>
                                <ul>
                                    <li><span>Call us on</span><a href="telto:1809090">Tel 1809090</a></li>
                                    <li><span>Write us on</span><a href="mailto:makanifoods@yasra.com">makanifoods@yasra.com</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </React.Fragment>
    );
}

export default NeedHelp;

