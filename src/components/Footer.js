import React from 'react';
import '../styles/Footer.scss';
const Footer = () => {
    return (
        <React.Fragment>
            <footer className="page-footer">
                <div className="footer content">
                    <div className="row">
                        <div className="footer-section col-md-12">
                            <div className="pagebuilder-column-group">
                                <div className="pagebuilder-column">
                                    <div className="footercontent">
                                        <div className="footer-item">
                                            <h3>
                                                <span>Contact Us</span>
                                            </h3>
                                            <p>Alyasra Foods CO. Head office
                                            <br />
                                            Plot 11, Block 8, Messilah Equestrian Club,
                                            <br />
                                            2 St, Sabhan Indl. Area,
                                            <br />Kuwait PO Box, 3228, Safat, 13033
                                            </p>
                                            <a href="tel:1809090">1809090</a>
                                        </div>
                                    </div>
                                </div>
                                <div className="pagebuilder-column">
                                    <div className="footmenu">
                                        <div className="footer-item">
                                            <h3>
                                                <span>Products</span>
                                            </h3>
                                            <ul>
                                                <li>
                                                    <a href="#">Chicken &amp; Poultry</a>
                                                </li>
                                                <li>
                                                    <a href="#">Beef &amp; Veal</a>
                                                </li>
                                                <li>
                                                    <a href="#">Seafood</a>
                                                </li>
                                                <li>
                                                    <a href="#">Sides, Fruits &amp; Vegetables</a>
                                                </li>
                                                <li>
                                                    <a href="#">Plant-based Alternatives</a>
                                                </li>
                                                <li>
                                                    <a href="#">Bread &amp; Desserts</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div className="pagebuilder-column">
                                    <div className="footern-about-menu">
                                        <div className="footer-item">
                                            <h3>
                                                <span>About</span>
                                            </h3>
                                            <ul>
                                                <li>
                                                    <a href="#">Locations</a>
                                                </li>
                                                <li>
                                                    <a href="#">FAQ</a>
                                                </li>
                                                <li>
                                                    <a href="#">Return Policy</a>
                                                </li>
                                                <li>
                                                    <a href="#">Terms &amp; Conditions</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div className="pagebuilder-column">
                                    <div className="footer-item">
                                        <h3>
                                            <span>Get fresh news in your inbox</span>
                                        </h3>
                                        <div className="block newsletter">
                                            <div className="title">
                                                <strong>Subscribe and receive our best offers in your inbox.</strong>
                                            </div>
                                            <div className="content">
                                                <form className="form subscribe" noValidate="novalidate" action="#" method="post" id="newsletter-validate-detail">
                                                    <div className="field newsletter">
                                                        <div className="control">
                                                            <label htmlFor="newsletter">
                                                                <span className="label">
                                                                    Sign Up for Our Newsletter:
                                                                     </span>
                                                                     <input name="email" type="email" id="newsletter" placeholder="Enter your e-mail address"/>
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div className="actions">
                                                        <button className="action subscribe primary" title="Subscribe" type="submit">
                                                            <span>Send</span>
                                                        </button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <small className="copyright">
                    <span> ©2020 Makani Foods</span>
                    <div className="row">
                        <div className="footer-copyright">
                            <div className="pagebuilder-column-group">
                                <div className="footer-copyright-text">
                                    <div className="menusec">
                                        <p>Product names, logos, brands, featured on Makani Foods’ website are the property of their respective trademark holders.</p>
                                    </div>
                                </div>
                                <div className="pagebuilder-column footer-socials-list">
                                    <div className="footer-social-icons">
                                    <ul>
                                        <li className="linkedin"><a href="https://www.linkedin.com/company/al-yasra-food-co-" target="_blank"><i className="fa fa-linkedin" aria-hidden="true"></i></a></li>
                                        <li className="insta"><a href="https://www.instagram.com/makanikw/" target="_blank"><i className="fa fa-instagram" aria-hidden="true"></i></a></li>
                                    </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </small>
                </div>
                <div className="back-top">
                    <a href="#" id="scroll">Top</a>
                </div>
                
            </footer>
        </React.Fragment >
    );
}


export default Footer;