import React from 'react';
import AppRoutes from './AppRoutes';
import { Route, BrowserRouter as Router} from 'react-router-dom';
import DeviceIdentifier from 'react-device-identifier';
import Header from './components/HeaderMenu/Header';
import MobileHeader from './components/MobileHeader';
import Footer from './components/Footer';

function App() {
  return (
    <Router>
      <React.Fragment>
        <DeviceIdentifier isDesktop={true}>
          <Header />
        </DeviceIdentifier>
        <DeviceIdentifier isMobile={true}>
          <MobileHeader/>
        </DeviceIdentifier>
        <DeviceIdentifier isTablet={true}>
        <Header />
        </DeviceIdentifier>
          <Route component={AppRoutes}/>
        <Footer />
      </React.Fragment>
    </Router>
  );
}

export default App;
