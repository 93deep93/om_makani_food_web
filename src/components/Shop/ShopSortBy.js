import React from 'react';
import ReactDOM from 'react-dom';
const ShopSortBy = () => {

    return (
        <React.Fragment>
            <div className="counter-sec">
                        <div className="total-product-counter">
                            <span>Products - Shop (178)</span>
                        </div>
                        <div className="toolbar toolbar-products">
                            <div className="toolbar-sorter sorter">
                                <label className="sorter-label" for="sorter">Sort By<i className="fa fa-angle-down" aria-hidden="true"></i></label>
                                <select id="sorter" data-role="sorter" className="sorter-options">
                                    <option value="name">
                                        Product Name
                                    </option>
                                    <option value="price">
                                        Price            
                                    </option>
                                </select>
                            </div>
                        </div>
                    </div>
        </React.Fragment>
    );
}

export default ShopSortBy;