import React from 'react';
import ReactDOM from 'react-dom';
const ShopGallery = () => {

    return (
        <React.Fragment>
            <div className="row gallery-images">
                        <div className="col-sm-4 gallery-img-holder">
                            <div className="product-wrapper">

                                <div className="product-wrapper-container">
                                    <a href="#">
                                        <img className="product-img" src={require('../../images/english-placeholder_1.png')} alt="" />
                                    </a>
                                    <div className="custom-icon"> <img className="product-img" src={require('../../images/weigh-scale.png')} alt="" /></div>
                                    <div className="product-hover">
                                        <ul>
                                            <li className="quickview">
                                                <a rel="nofollow" className="weltpixel-quickview weltpixel_quickview_button_v2" href="javascript:void(0);"></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div className="product-desc">
                                    <div className="customize-icon">

                                        <a href="#">
                                            <i className="fa fa-balance-scale" aria-hidden="true"></i>
                                            <span>Customise</span>
                                        </a>

                                    </div>
                                    <h4 className="product-name">
                                        <a href="#">Beef Breakfast Slices - Chefkom</a>
                                    </h4>
                                    <div className="review-container">
                                        <div className="product-reviews-summary short">
                                            <div className="rating-summary">
                                                <span className="label">
                                                    <span>Rating:</span></span>
                                                <div className="rating-result" title="93%">
                                                    <span style={{ width: "93%" }}><span>93%</span></span>
                                                </div>
                                            </div>
                                            <div className="reviews-actions">
                                                <a className="action view" href="#">4&nbsp;<span>Reviews</span></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="product-price">
                                        <div className="price-box price-final_price">
                                            <span className="price-container price-final_price tax weee">
                                                <span id="product-price-289" className="price-wrapper ">
                                                    <span className="price">KWD 5.700</span>
                                                </span>
                                            </span>
                                            <span className="average-weight">
                                                Average Weight 2.5 to 3.5 Kg
                                            </span>
                                        </div>
                                    </div>
                                    <div className="product-items-actions">
                                        <div className="add-to-cart actions-primary">
                                            <form action="#" method="post">
                                                <div className="field qty">
                                                    <div className="control qty-change">
                                                        <button className="decreaseqty" id="344-dec">-</button>
                                                        <input readonly="" type="text" name="qty" value="1" title="Qty" className="input-text qty" />
                                                        <button className="increaseqty" id="344-upt">+</button>
                                                    </div>
                                                </div>
                                                <div className="custom-option-container"
                                                ><div className="custom-options">
                                                        <label>Selected Weight </label>
                                                        <select name="options">
                                                            <option value="1">1kg</option>
                                                            <option value="2">2kg</option>
                                                            <option value="3">3kg</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <button type="submit" title="Add to Cart" className="action tocart primary">
                                                    <span>Add to Cart</span>
                                                </button>

                                            </form>
                                        </div>
                                        <div className="actions-secondary wishlist">
                                            <a href="#" className="action towishlist" title="Add to Wish List" role="button">
                                                <span>Add to Wish List</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div className="col-sm-4 gallery-img-holder">
                            <div className="product-wrapper">

                                <div className="product-wrapper-container">
                                    <a href="#">
                                        <img className="product-img" src={require('../../images/english-placeholder_1.png')} alt="" />
                                    </a>
                                    <div className="custom-icon"> <img className="product-img" src={require('../../images/weigh-scale.png')} alt="" /></div>
                                    <div className="product-hover">
                                        <ul>
                                            <li className="quickview">
                                                <a rel="nofollow" className="weltpixel-quickview weltpixel_quickview_button_v2" href="javascript:void(0);"></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div className="product-desc">
                                    <div className="customize-icon">

                                        <a href="#">
                                            <i className="fa fa-balance-scale" aria-hidden="true"></i>
                                            <span>Customise</span>
                                        </a>

                                    </div>
                                    <h4 className="product-name">
                                        <a href="#">Beef Breakfast Slices - Chefkom</a>
                                    </h4>
                                    <div className="review-container">
                                        <div className="product-reviews-summary short">
                                            <div className="rating-summary">
                                                <span className="label">
                                                    <span>Rating:</span></span>
                                                <div className="rating-result" title="93%">
                                                    <span style={{ width: "93%" }}><span>93%</span></span>
                                                </div>
                                            </div>
                                            <div className="reviews-actions">
                                                <a className="action view" href="#">4&nbsp;<span>Reviews</span></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="product-price">
                                        <div className="price-box price-final_price">
                                            <span className="price-container price-final_price tax weee">
                                                <span id="product-price-289" className="price-wrapper ">
                                                    <span className="price">KWD 5.700</span>
                                                </span>
                                            </span>
                                            <span className="average-weight">
                                                Average Weight 2.5 to 3.5 Kg
                                            </span>
                                        </div>
                                    </div>
                                    <div className="product-items-actions">
                                        <div className="add-to-cart actions-primary">
                                            <form action="#" method="post">
                                                <div className="field qty">
                                                    <div className="control qty-change">
                                                        <button className="decreaseqty" id="344-dec">-</button>
                                                        <input readonly="" type="text" name="qty" value="1" title="Qty" className="input-text qty" />
                                                        <button className="increaseqty" id="344-upt">+</button>
                                                    </div>
                                                </div>
                                                <div className="custom-option-container"
                                                ><div className="custom-options">
                                                        <label>Selected Weight </label>
                                                        <select name="options">
                                                            <option value="1">1kg</option>
                                                            <option value="2">2kg</option>
                                                            <option value="3">3kg</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <button type="submit" title="Add to Cart" className="action tocart primary">
                                                    <span>Add to Cart</span>
                                                </button>

                                            </form>
                                        </div>
                                        <div className="actions-secondary wishlist">
                                            <a href="#" className="action towishlist" title="Add to Wish List" role="button">
                                                <span>Add to Wish List</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div className="col-sm-4 gallery-img-holder">
                            <div className="product-wrapper">

                                <div className="product-wrapper-container">
                                    <a href="#">
                                        <img className="product-img" src={require('../../images/english-placeholder_1.png')} alt="" />
                                    </a>

                                    <div className="product-hover">
                                        <ul>
                                            <li className="quickview">
                                                <a rel="nofollow" className="weltpixel-quickview weltpixel_quickview_button_v2" href="javascript:void(0);"></a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div className="product_sticker">
                                        <span className="sticker-label Organic">Organic</span>
                                        <span className="sticker-label Milk Fed">Milk Fed</span>
                                    </div>
                                </div>
                                <div className="product-desc">

                                    <h4 className="product-name">
                                        <a href="#">Beef Breakfast Slices - Chefkom</a>
                                    </h4>
                                    <div className="review-container">
                                        <div className="product-reviews-summary short">
                                            <div className="rating-summary">
                                                <span className="label">
                                                    <span>Rating:</span></span>
                                                <div className="rating-result" title="93%">
                                                    <span style={{ width: "93%" }}><span>93%</span></span>
                                                </div>
                                            </div>
                                            <div className="reviews-actions">
                                                <a className="action view" href="#">4&nbsp;<span>Reviews</span></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="product-price">
                                        <div className="price-box price-final_price">
                                            <span className="price-container price-final_price tax weee">
                                                <span id="product-price-289" className="price-wrapper ">
                                                    <span className="price">KWD 5.700</span>
                                                </span>
                                            </span>
                                            <span className="average-weight">
                                                Average Weight 2.5 to 3.5 Kg
                                            </span>
                                        </div>
                                    </div>
                                    <div className="product-items-actions">
                                        <div className="add-to-cart actions-primary">
                                            <form action="#" method="post">
                                                <div className="field qty">
                                                    <div className="control qty-change">
                                                        <button className="decreaseqty" id="344-dec">-</button>
                                                        <input readonly="" type="text" name="qty" value="1" title="Qty" className="input-text qty" />
                                                        <button className="increaseqty" id="344-upt">+</button>
                                                    </div>
                                                </div>

                                                <button type="submit" title="Add to Cart" className="action tocart primary">
                                                    <span>Add to Cart</span>
                                                </button>

                                            </form>
                                        </div>
                                        <div className="actions-secondary wishlist">
                                            <a href="#" className="action towishlist" title="Add to Wish List" role="button">
                                                <span>Add to Wish List</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
        </React.Fragment>
    );
}

export default ShopGallery;