import React from 'react';
import DeviceIdentifier from 'react-device-identifier';
import Banner from './Home/Banner';
import BannerMobile from './Home/BannerMobile';
import ProductsHome from './Home/ProductsHome';
import PoultryRange from './Home/PoultryRange';
import NewsletterHome from './Home/NewsletterHome';
import FindDoorstep from './Home/FindDoorstep';
function Home() {
    return (
      <React.Fragment>
            <DeviceIdentifier isDesktop={true}>
              <Banner />
            </DeviceIdentifier>
            <DeviceIdentifier isMobile={true}>
              <BannerMobile />
          </DeviceIdentifier>
          <DeviceIdentifier isTablet={true}>
            <BannerMobile />
          </DeviceIdentifier>
          <ProductsHome />
          <PoultryRange />
          <NewsletterHome />
          <FindDoorstep />
      </React.Fragment>
    );
  }
  
  export default Home;
  