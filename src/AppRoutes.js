import React from 'react';
import logo from './logo.svg';
import { Switch, Route } from 'react-router-dom';
import './App.css';
import './styles/Fonts.scss';
import Shop from './Pages/Shop';
import ProductsDetails from './components/Products/ProductsDetails';
import Home from './components/Home';
import HowitWorks from './components/How-it-Works';
import ShowroomLocations from './components/ShowroomLocations';
function AppRoutes() {
  return (
    <React.Fragment>
        <Switch>
            <Route exact path='/' component={Home} />
            <Route path ="/Shop" component={Shop} />
            <Route path ="/single-product" component={ProductsDetails} />
            <Route path ="/how-it-works" component={HowitWorks} />
            <Route path ="/showroom-locations" component={ShowroomLocations} />
        </Switch>
    </React.Fragment>
  );
}

export default AppRoutes;
