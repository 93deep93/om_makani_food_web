import React, { useEffect, useState } from 'react';
import ReactDOM from 'react-dom';
import "../styles/Shop.scss";
import BreadCrumbs from '../components/Assets/BreadCrumbs';
import NeedHelp from '../components/Need-Help';
import ShopGallery from '../components/Shop/shopGallery';
import { getProductsCollections } from '../Api/commonApi';
import ShopProductList from '../components/Shop/shopProductList';
const Shop = () => {
    const [ productCollects, setProductCollects ] = useState([]);
    const [ showCollectToggle, setShowCollectToggle ] = useState(false);
    const [ showPriceToggle, setShowPriceToggle ] = useState(false);
    const [ default_sort_by, setDefaultSortBy ] = useState('product_name');
    useEffect(() => {
        // code to run on component mount
       
        getProductsCollections( )
          .then(res => {
            const productCollect = res.hits.hits.reduce((result, cat, index)=>{
              result.push(cat._source);
              return result;
            },[]);
            setProductCollects(productCollect);
          });
      }, [])

    return (
        <React.Fragment>
            <BreadCrumbs />
            <div className="page-title-wrapper container">
                <h1 className="page-title" id="page-title-heading">
                    <span className="base">Shop</span>
                </h1>
            </div>
            <div className="product-display shop-page-product">
                <div className="container">
                    <div className="block filter">
                        <div className="block-content filter-content">
                            <div className="block-title filter-title">
                                <strong>Filter By</strong>
                            </div>

                            <dl className="filter-options" id="narrow-by-list">
                                <div className="filter-list">
                                    <dt onClick={()=>{ setShowCollectToggle(!showCollectToggle); if(showPriceToggle === true)setShowPriceToggle(false)}} role="heading" className="filter-options-title">Category</dt>
                                    <dd style={{ display : showCollectToggle ? 'block' : 'none' }} className="filter-options-content">
                                        <ol className="items">
                                            <li className="item">
                                                <a href="#">
                                                    Collections<span className="count">( {productCollects.length})<span className="filter-count-label"> </span></span>
                                                </a>
                                            </li>
                                        </ol>
                                    </dd>
                                </div>
                                <div className="filter-list">
                                    <dt onClick={()=>{ setShowPriceToggle(!showPriceToggle); if(showCollectToggle === true)setShowCollectToggle(false) }} role="heading" aria-level="3" className="filter-options-title">Price</dt>
                                    <dd style={{ display : showPriceToggle ? 'block' : 'none' }}  className="filter-options-content">
                                        <ol className="items">
                                            <li className="item">
                                                <a href="#">
                                                    <span className="price">KWD 0.000</span> - <span className="price">KWD 9.990</span>
                                                    <span className="count">164<span className="filter-count-label">
                                                        item</span></span>
                                                </a>
                                            </li>
                                            <li className="item">
                                                <a href="#">
                                                    <span className="price">KWD 10.000</span> - <span className="price">KWD 19.990</span>
                                                     <span className="count">11<span className="filter-count-label">
                                                        item</span></span>
                                                </a>
                                            </li>
                                            <li className="item">
                                                <a href="#">
                                                    <span className="price">KWD 20.000</span> - <span className="price">KWD 29.990</span>
                                                    <span className="count">2<span className="filter-count-label">
                                                        item</span></span>
                                                </a>
                                            </li>
                                            <li className="item">
                                                <a href="#">
                                                    <span className="price">KWD 50.000</span> and above 
                                                    <span className="count">1<span className="filter-count-label">
                                                        item</span></span>
                                                </a>
                                            </li>
                                        </ol>
                                    </dd>
                                </div>
                            </dl>
                        </div>
                    </div>

                    <ShopProductList/>
                    
                    <ShopGallery />
                </div>
                <div className="shop-all-button">
                    <a href="#">Shop More</a>
                </div>
            </div>
            <NeedHelp/>
        </React.Fragment>
    );
}

export default Shop;

