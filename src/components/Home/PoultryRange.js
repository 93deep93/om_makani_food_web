import React from 'react';
import ReactDOM from 'react-dom';
import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
import '../../styles/PoultryRange.scss';

const PoultryRange = () => {
    const options = {
        margin: 10,
        responsiveClass: true,
        nav: false,
        dots: false,
        autoplay: true,
        autoplayTimeout: 7000,
        smartSpeed: 1000,
        responsive: {
            0: {
                items: 1,
            },
            400: {
                items: 1,
            },
            600: {
                items: 2,
            },
            700: {
                items: 2,
            },
            1000: {
                items: 3,
    
            }
        },
    };
    return (
        <React.Fragment>
            <section className="explres-sec">
                <div className="container">
                    <div className="row">
                        <div className="col-md-4">
                            <div className="pultry-explore">
                                <div className="title">
                                    <h3><span>Explore the Widest Poultry Range in Town</span></h3>
                                </div>
                                <a className="shop-btn" href="javascript:void(0)">Shop Poultry</a>

                                <div className="poutryimage"></div>
                            </div>
                        </div>
                        <div className="col-md-8">
                            <div className="produts-display">
                                <div className="handpciked-title">
                                    <h3><span>Hand Picked Poultry</span></h3>
                                </div>
                            <div className="product-solution">
                                <div className="product-filter">
                                    <ul className="nav nav-pills gallery-filters">
                                        <li role="presentation" className="active" >
                                            <a href="#">New products</a>
                                        </li>
                                        <li role="presentation">
                                            <a href="#">Best Sellers</a>
                                        </li>
                                        <li role="presentation">
                                            <a href="#">Best Deals</a>
                                        </li>
                                        <li role="presentation">
                                            <a href="#">Organic</a>
                                        </li>
                                    </ul>
                                </div>
                    
                                <div className="product-slider">
                                    <OwlCarousel
                                        {...options}
                                        
                                    >
                                        <div className="item">
                                            <div className="gallery-img-holder">
                                                <div className="product-wrapper">

                                                    <div className="product-wrapper-container">
                                                        <a href="#">
                                                            <img className="product-img" src={require('../../images/english-placeholder_1.png')} alt="" />
                                                        </a>
                                                        <div className="product-hover">
                                                            <ul>
                                                                <li className="wishlist">
                                                                    <a href="#" className="action towishlist">
                                                                    </a>
                                                                </li>
                                                                <li className="quickview">
                                                                    <a rel="nofollow" className="weltpixel-quickview weltpixel_quickview_button_v2" href="javascript:void(0);"></a>
                                                                </li>
                                                                <li className="add-to-cart">
                                                                    <div className="actions add-to-cart">
                                                                        <button type="submit" title="Add to Bag" className="action tocart primary">
                                                                            <span>Add to Bag</span>
                                                                        </button>
                                                                        <div className="product-item-inner">
                                                                            <button type="submit" title="Add to Bag" className="action tocart primary"></button>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                            </ul>

                                                        </div>

                                                    </div>
                                                    <div className="product-desc">
                                                        <h4 className="product-name">
                                                            <a href="#">Beef Breakfast Slices - Chefkom</a>
                                                        </h4>
                                                        <div className="product-price">
                                                            <div className="price-box price-final_price">
                                                                <span className="price-container price-final_price tax weee">
                                                                    <span id="product-price-289" className="price-wrapper ">
                                                                        <span className="price">KWD 5.700</span>
                                                                    </span>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <div className="item">
                                            <div className="gallery-img-holder">
                                                <div className="product-wrapper">

                                                    <div className="product-wrapper-container">
                                                        <a href="#">
                                                            <img className="product-img" src={require('../../images/english-placeholder_1.png')} alt="" />
                                                        </a>
                                                        <div className="product-hover">
                                                            <ul>
                                                                <li className="wishlist">
                                                                    <a href="#" className="action towishlist">
                                                                    </a>
                                                                </li>
                                                                <li className="quickview">
                                                                    <a rel="nofollow" className="weltpixel-quickview weltpixel_quickview_button_v2" href="javascript:void(0);"></a>
                                                                </li>
                                                                <li className="add-to-cart">
                                                                    <div className="actions add-to-cart">
                                                                        <button type="submit" title="Add to Bag" className="action tocart primary">
                                                                            <span>Add to Bag</span>
                                                                        </button>
                                                                        <div className="product-item-inner">
                                                                            <button type="submit" title="Add to Bag" className="action tocart primary"></button>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                            </ul>

                                                        </div>

                                                    </div>
                                                    <div className="product-desc">
                                                        <h4 className="product-name">
                                                            <a href="#">Beef Breakfast Slices - Chefkom</a>
                                                        </h4>
                                                        <div className="product-price">
                                                            <div className="price-box price-final_price">
                                                                <span className="price-container price-final_price tax weee">
                                                                    <span id="product-price-289" className="price-wrapper ">
                                                                        <span className="price">KWD 5.700</span>
                                                                    </span>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <div className="item">
                                            <div className="gallery-img-holder">
                                                <div className="product-wrapper">

                                                    <div className="product-wrapper-container">
                                                        <a href="#">
                                                            <img className="product-img" src={require('../../images/english-placeholder_1.png')} alt="" />
                                                        </a>
                                                        <div className="product-hover">
                                                            <ul>
                                                                <li className="wishlist">
                                                                    <a href="#" className="action towishlist">
                                                                    </a>
                                                                </li>
                                                                <li className="quickview">
                                                                    <a rel="nofollow" className="weltpixel-quickview weltpixel_quickview_button_v2" href="javascript:void(0);"></a>
                                                                </li>
                                                                <li className="add-to-cart">
                                                                    <div className="actions add-to-cart">
                                                                        <button type="submit" title="Add to Bag" className="action tocart primary">
                                                                            <span>Add to Bag</span>
                                                                        </button>
                                                                        <div className="product-item-inner">
                                                                            <button type="submit" title="Add to Bag" className="action tocart primary"></button>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                            </ul>

                                                        </div>

                                                    </div>
                                                    <div className="product-desc">
                                                        <h4 className="product-name">
                                                            <a href="#">Beef Breakfast Slices - Chefkom</a>
                                                        </h4>
                                                        <div className="product-price">
                                                            <div className="price-box price-final_price">
                                                                <span className="price-container price-final_price tax weee">
                                                                    <span id="product-price-289" className="price-wrapper ">
                                                                        <span className="price">KWD 5.700</span>
                                                                    </span>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <div className="item">
                                            <div className="gallery-img-holder">
                                                <div className="product-wrapper">

                                                    <div className="product-wrapper-container">
                                                        <a href="#">
                                                            <img className="product-img" src={require('../../images/english-placeholder_1.png')} alt="" />
                                                        </a>
                                                        <div className="product-hover">
                                                            <ul>
                                                                <li className="wishlist">
                                                                    <a href="#" className="action towishlist">
                                                                    </a>
                                                                </li>
                                                                <li className="quickview">
                                                                    <a rel="nofollow" className="weltpixel-quickview weltpixel_quickview_button_v2" href="javascript:void(0);"></a>
                                                                </li>
                                                                <li className="add-to-cart">
                                                                    <div className="actions add-to-cart">
                                                                        <button type="submit" title="Add to Bag" className="action tocart primary">
                                                                            <span>Add to Bag</span>
                                                                        </button>
                                                                        <div className="product-item-inner">
                                                                            <button type="submit" title="Add to Bag" className="action tocart primary"></button>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                            </ul>

                                                        </div>

                                                    </div>
                                                    <div className="product-desc">
                                                        <h4 className="product-name">
                                                            <a href="#">Beef Breakfast Slices - Chefkom</a>
                                                        </h4>
                                                        <div className="product-price">
                                                            <div className="price-box price-final_price">
                                                                <span className="price-container price-final_price tax weee">
                                                                    <span id="product-price-289" className="price-wrapper ">
                                                                        <span className="price">KWD 5.700</span>
                                                                    </span>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>

                                    </OwlCarousel>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>


        </React.Fragment>
    );
}

export default PoultryRange;