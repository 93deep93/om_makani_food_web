import React from 'react';
import ReactDOM from 'react-dom';
import "../styles/HowitWorks.scss";
import NeedHelp from '../components/Need-Help';
import BreadCrumbs from './Assets/BreadCrumbs';
const HowitWorks = () => {

    return (
        <React.Fragment>
            <BreadCrumbs />
            <div className="how-it-works">
            <div className="page-title-wrapper container">
                <h1 className="page-title" id="page-title-heading">
                    <span className="base">How It works</span>
                </h1>
                <div class="subtext">
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                </div>
            </div>
            <section className="how-it-works">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="how-it-image-sec">
                                <div class="image">
                                    <img class="shop" src={require('../images/shop-how-it-works.svg')} alt="Shop" />
                                </div>
                                <div class="list-description">
                                    <div class="title">
                                        <h3>1. Shop</h3>
                                    </div>
                                    <div class="description">
                                        <p>Test 1 Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                       
                        <div class="col-md-6">
                            <div class="how-it-image-sec">
                                <div class="image">
                                    <img class="shop" src={require('../images/recieve-how-it-works.svg')} alt="Recieve" />
                                </div>
                                <div class="list-description">
                                    <div class="title">
                                        <h3>2. Recieve</h3>
                                    </div>
                                    <div class="description">
                                        <p> Test 1 Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="how-it-image-sec">
                                <div class="image">
                                <img class="shop" src={require('../images/cook-how-it-works.svg')} alt="Cook" />
                            
                                </div>
                                <div class="list-description">
                                    <div class="title">
                                        <h3>3. Cook</h3>
                                    </div>
                                    <div class="description">
                                        <p> Test 1 Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="how-it-image-sec">
                                <div class="image">
                                <img class="shop" src={require('../images/enjoy-how-it-works.svg')} alt="Enjoy" />
                                </div>
                                <div class="list-description">
                                    <div class="title">
                                        <h3>4.Enjoy</h3>
                                    </div>
                                    <div class="description">
                                        <p> test 2 Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <div className="product-display how-it-works-product">
                <div className="container">  
                    <div className="row">
                        <div class="col-md-12">
                        <div class="block-title title">
                            <h2>You May Also Like</h2>
                            <span>Related Products others have bought with beef Sirloin</span>
                        </div>
                        </div>
                    </div>
                    <div className="row gallery-images">
                        <div className="col-sm-3 gallery-img-holder">
                            <div className="product-wrapper">

                                <div className="product-wrapper-container">
                                    <a href="#">
                                        <img className="product-img" src={require('../images/english-placeholder_1.png')} alt="" />
                                    </a>
                                    
                                    <div className="product-hover">
                                        <ul>
                                            <li className="wishlist"><a href="#" class="action towishlist"></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div className="product-desc">
                                   
                                    <h4 className="product-name">
                                        <a href="#">Beef Breakfast Slices - Chefkom</a>
                                    </h4>
                                    
                                    <div className="product-price">
                                        <div className="price-box price-final_price">
                                            <span className="price-container price-final_price tax weee">
                                                <span id="product-price-289" className="price-wrapper ">
                                                    <span className="price">KWD 5.700</span>
                                                </span>
                                            </span>
                                            <span className="average-weight">
                                                Average Weight 2.5 to 3.5 Kg
                                            </span>
                                        </div>
                                    </div>
                                    <div className="product-items-actions">
                                        <div className="add-to-cart actions-primary">
                                            <form action="#" method="post">
                                              
                                                <button type="submit" title="Add to Cart" className="action tocart primary">
                                                    <span>Add to Cart</span>
                                                </button>

                                            </form>
                                        </div>
                    
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div className="col-sm-3 gallery-img-holder">
                            <div className="product-wrapper">

                                <div className="product-wrapper-container">
                                    <a href="#">
                                        <img className="product-img" src={require('../images/english-placeholder_1.png')} alt="" />
                                    </a>
                                    
                                    <div className="product-hover">
                                        <ul>
                                            <li className="wishlist"><a href="#" class="action towishlist"></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div className="product-desc">
                                   
                                    <h4 className="product-name">
                                        <a href="#">Beef Breakfast Slices - Chefkom</a>
                                    </h4>
                                    
                                    <div className="product-price">
                                        <div className="price-box price-final_price">
                                            <span className="price-container price-final_price tax weee">
                                                <span id="product-price-289" className="price-wrapper ">
                                                    <span className="price">KWD 5.700</span>
                                                </span>
                                            </span>
                                            <span className="average-weight">
                                                Average Weight 2.5 to 3.5 Kg
                                            </span>
                                        </div>
                                    </div>
                                    <div className="product-items-actions">
                                        <div className="add-to-cart actions-primary">
                                            <form action="#" method="post">
                                              
                                                <button type="submit" title="Add to Cart" className="action tocart primary">
                                                    <span>Add to Cart</span>
                                                </button>

                                            </form>
                                        </div>
                    
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div className="col-sm-3 gallery-img-holder">
                            <div className="product-wrapper">

                                <div className="product-wrapper-container">
                                    <a href="#">
                                        <img className="product-img" src={require('../images/english-placeholder_1.png')} alt="" />
                                    </a>
                                    
                                    <div className="product-hover">
                                        <ul>
                                            <li className="wishlist"><a href="#" class="action towishlist"></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div className="product-desc">
                                   
                                    <h4 className="product-name">
                                        <a href="#">Beef Breakfast Slices - Chefkom</a>
                                    </h4>
                                    
                                    <div className="product-price">
                                        <div className="price-box price-final_price">
                                            <span className="price-container price-final_price tax weee">
                                                <span id="product-price-289" className="price-wrapper ">
                                                    <span className="price">KWD 5.700</span>
                                                </span>
                                            </span>
                                            <span className="average-weight">
                                                Average Weight 2.5 to 3.5 Kg
                                            </span>
                                        </div>
                                    </div>
                                    <div className="product-items-actions">
                                        <div className="add-to-cart actions-primary">
                                            <form action="#" method="post">
                                              
                                                <button type="submit" title="Add to Cart" className="action tocart primary">
                                                    <span>Add to Cart</span>
                                                </button>

                                            </form>
                                        </div>
                    
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div className="col-sm-3 gallery-img-holder">
                            <div className="product-wrapper">

                                <div className="product-wrapper-container">
                                    <a href="#">
                                        <img className="product-img" src={require('../images/english-placeholder_1.png')} alt="" />
                                    </a>
                                    
                                    <div className="product-hover">
                                        <ul>
                                            <li className="wishlist"><a href="#" class="action towishlist"></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div className="product-desc">
                                   
                                    <h4 className="product-name">
                                        <a href="#">Beef Breakfast Slices - Chefkom</a>
                                    </h4>
                                    
                                    <div className="product-price">
                                        <div className="price-box price-final_price">
                                            <span className="price-container price-final_price tax weee">
                                                <span id="product-price-289" className="price-wrapper ">
                                                    <span className="price">KWD 5.700</span>
                                                </span>
                                            </span>
                                            <span className="average-weight">
                                                Average Weight 2.5 to 3.5 Kg
                                            </span>
                                        </div>
                                    </div>
                                    <div className="product-items-actions">
                                        <div className="add-to-cart actions-primary">
                                            <form action="#" method="post">
                                              
                                                <button type="submit" title="Add to Cart" className="action tocart primary">
                                                    <span>Add to Cart</span>
                                                </button>

                                            </form>
                                        </div>
                    
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>             
            </div>
            <NeedHelp />
            </div>
        </React.Fragment>
    );
}

export default HowitWorks;

