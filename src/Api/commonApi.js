import axios from 'axios';
import  {ORIGIN_BASE_URL} from './index';

export function getProductsCategories() {
    return axios.get(`${ORIGIN_BASE_URL}api/catalog/vue_storefront_catalog/category/_search`)
            .then(response => response.data);
}


export function getProductsCollections() {
    return axios.get(`${ORIGIN_BASE_URL}api/catalog/vue_storefront_catalog/product/_search`)
            .then(response => response.data);
}
