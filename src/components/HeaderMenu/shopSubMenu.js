import React, { useEffect, useState } from 'react';
import ProductSubMenu from './productSubMenu';

const PRODUCT_PARENT_ID = 12
const CCOLLECTION_PARENT_ID = 0
const  ShopSubMenu =( props)=> {
    const [ showSubMenu , setShowSubMenu ] = useState('products')
    const [ productCategories , setProductCategories ] = useState([]);
    const [ collectionCategories , setCollectionCategories ] = useState([]);

    useEffect(( ) => {
        if(props.categories && props.categories.length > 0){
            const productCategories = props.categories.filter((cat) =>{
                if( cat.parent_id == PRODUCT_PARENT_ID && cat.is_active == true)
                return cat
            });
            setProductCategories(productCategories);
            const collectionCategories = props.categories.filter((cat) =>{
                if( cat.parent_id == CCOLLECTION_PARENT_ID && cat.is_active === true)
                return cat
            });
            setCollectionCategories(collectionCategories);
        }
    }, [props]);

    return (
      <React.Fragment>
       <div className="menu-left">
            <ul className="menu-links">
            <li className={"menu-parent "+ (showSubMenu === 'products'? 'active' :'')}>
                <a onClick={()=> setShowSubMenu('products')}>Products</a>
                < ProductSubMenu  productCategories={productCategories}/>
            </li>
            <li className={"menu-parent "+ (showSubMenu === 'collections'? 'active' :'')}>
                <a onClick={()=> setShowSubMenu('collections') }>Collections</a>
                <div className="menu-right">
                <ul className="menu-links-right">
                    <li className="first-level">
                    <ul>
                        <li>
                            <a href="#">Organic</a>
                        </li>
                    </ul>
                    </li>
                </ul>
                </div>
            </li>
            </ul>
        </div>
      </React.Fragment>
    );
  }
  
  export default ShopSubMenu;
  