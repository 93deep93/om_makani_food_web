import React from 'react';
import ReactDOM from 'react-dom';
import "../styles/Shop.scss";
import BreadCrumbs from '../components/BreadCrumbs';
import NeedHelp from '../components/Need-Help';
const Shop = () => {

    return (
        <React.Fragment>
            <BreadCrumbs />
            <div className="page-title-wrapper container">
                <h1 className="page-title" id="page-title-heading">
                    <span className="base">Shop</span>
                </h1>
            </div>
            <div className="product-display shop-page-product">
                <div className="container">
                    <div className="block filter">
                        <div className="block-content filter-content">
                            <div className="block-title filter-title">
                                <strong>Filter By</strong>
                            </div>

                            <dl className="filter-options" id="narrow-by-list">
                                <div className="filter-list">
                                    <dt role="heading" className="filter-options-title">Category</dt>
                                    <dd className="filter-options-content">
                                        <ol className="items">
                                            <li className="item">
                                                <a href="#">
                                                    Collections<span className="count">7<span className="filter-count-label"> item</span></span>
                                                </a>
                                            </li>
                                        </ol>
                                    </dd>
                                </div>
                                <div className="filter-list">
                                    <dt role="heading" aria-level="3" className="filter-options-title">Price</dt>
                                    <dd className="filter-options-content">
                                        <ol className="items">
                                            <li className="item">
                                                <a href="#">
                                                    <span className="price">KWD 0.000</span> - <span className="price">KWD 9.990</span>                                            <span className="count">164<span className="filter-count-label">
                                                        item</span></span>
                                                </a>
                                            </li>
                                            <li className="item">
                                                <a href="#">
                                                    <span className="price">KWD 10.000</span> - <span className="price">KWD 19.990</span>                                            <span className="count">11<span className="filter-count-label">
                                                        item</span></span>
                                                </a>
                                            </li>
                                            <li className="item">
                                                <a href="#">
                                                    <span className="price">KWD 20.000</span> - <span className="price">KWD 29.990</span>                                            <span className="count">2<span className="filter-count-label">
                                                        item</span></span>
                                                </a>
                                            </li>
                                            <li className="item">
                                                <a href="#">
                                                    <span className="price">KWD 50.000</span> and above                                            <span className="count">1<span className="filter-count-label">
                                                        item</span></span>
                                                </a>
                                            </li>
                                        </ol>
                                    </dd>
                                </div>
                            </dl>
                        </div>
                    </div>
                    <div className="counter-sec">
                        <div className="total-product-counter">
                            <span>Products - Shop (178)</span>
                        </div>
                        <div className="toolbar toolbar-products">
                           
                            <div className="toolbar-sorter sorter">
                                <label className="sorter-label" for="sorter">Sort By<i className="fa fa-angle-down" aria-hidden="true"></i></label>
                                <select id="sorter" data-role="sorter" className="sorter-options">
                                    <option value="name">
                                        Product Name
                                    </option>
                                    <option value="price">
                                        Price            
                                    </option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div className="row gallery-images">
                        <div className="col-sm-4 col-lg-3 gallery-img-holder">
                            <div className="product-wrapper">

                                <div className="product-wrapper-container">
                                    <a href="#">
                                        <img className="product-img" src={require('../images/english-placeholder_1.png')} alt="" />
                                    </a>
                                    <div className="custom-icon"> <img className="product-img" src={require('../images/weigh-scale.png')} alt="" /></div>
                                    <div className="product-hover">
                                        <ul>
                                            <li className="quickview">
                                                <a rel="nofollow" className="weltpixel-quickview weltpixel_quickview_button_v2" href="javascript:void(0);"></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div className="product-desc">
                                    <div className="customize-icon">

                                        <a href="#">
                                            <i className="fa fa-balance-scale" aria-hidden="true"></i>
                                            <span>Customise</span>
                                        </a>

                                    </div>
                                    <h4 className="product-name">
                                        <a href="#">Beef Breakfast Slices - Chefkom</a>
                                    </h4>
                                    <div className="review-container">
                                        <div className="product-reviews-summary short">
                                            <div className="rating-summary">
                                                <span className="label">
                                                    <span>Rating:</span></span>
                                                <div className="rating-result" title="93%">
                                                    <span style={{ width: "93%" }}><span>93%</span></span>
                                                </div>
                                            </div>
                                            <div className="reviews-actions">
                                                <a className="action view" href="#">4&nbsp;<span>Reviews</span></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="product-price">
                                        <div className="price-box price-final_price">
                                            <span className="price-container price-final_price tax weee">
                                                <span id="product-price-289" className="price-wrapper ">
                                                    <span className="price">KWD 5.700</span>
                                                </span>
                                            </span>
                                            <span className="average-weight">
                                                Average Weight 2.5 to 3.5 Kg
                                            </span>
                                        </div>
                                    </div>
                                    <div className="product-items-actions">
                                        <div className="add-to-cart actions-primary">
                                            <form action="#" method="post">
                                                <div className="field qty">
                                                    <div className="control qty-change">
                                                        <button className="decreaseqty" id="344-dec">-</button>
                                                        <input readonly="" type="text" name="qty" value="1" title="Qty" className="input-text qty" />
                                                        <button className="increaseqty" id="344-upt">+</button>
                                                    </div>
                                                </div>
                                                <div className="custom-option-container"
                                                ><div className="custom-options">
                                                        <label>Selected Weight </label>
                                                        <select name="options">
                                                            <option value="1">1kg</option>
                                                            <option value="2">2kg</option>
                                                            <option value="3">3kg</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <button type="submit" title="Add to Cart" className="action tocart primary">
                                                    <span>Add to Cart</span>
                                                </button>

                                            </form>
                                        </div>
                                        <div className="actions-secondary wishlist">
                                            <a href="#" className="action towishlist" title="Add to Wish List" role="button">
                                                <span>Add to Wish List</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div className="col-sm-4 col-lg-3 gallery-img-holder">
                            <div className="product-wrapper">

                                <div className="product-wrapper-container">
                                    <a href="#">
                                        <img className="product-img" src={require('../images/english-placeholder_1.png')} alt="" />
                                    </a>
                                    <div className="custom-icon"> <img className="product-img" src={require('../images/weigh-scale.png')} alt="" /></div>
                                    <div className="product-hover">
                                        <ul>
                                            <li className="quickview">
                                                <a rel="nofollow" className="weltpixel-quickview weltpixel_quickview_button_v2" href="javascript:void(0);"></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div className="product-desc">
                                    <div className="customize-icon">

                                        <a href="#">
                                            <i className="fa fa-balance-scale" aria-hidden="true"></i>
                                            <span>Customise</span>
                                        </a>

                                    </div>
                                    <h4 className="product-name">
                                        <a href="#">Beef Breakfast Slices - Chefkom</a>
                                    </h4>
                                    <div className="review-container">
                                        <div className="product-reviews-summary short">
                                            <div className="rating-summary">
                                                <span className="label">
                                                    <span>Rating:</span></span>
                                                <div className="rating-result" title="93%">
                                                    <span style={{ width: "93%" }}><span>93%</span></span>
                                                </div>
                                            </div>
                                            <div className="reviews-actions">
                                                <a className="action view" href="#">4&nbsp;<span>Reviews</span></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="product-price">
                                        <div className="price-box price-final_price">
                                            <span className="price-container price-final_price tax weee">
                                                <span id="product-price-289" className="price-wrapper ">
                                                    <span className="price">KWD 5.700</span>
                                                </span>
                                            </span>
                                            <span className="average-weight">
                                                Average Weight 2.5 to 3.5 Kg
                                            </span>
                                        </div>
                                    </div>
                                    <div className="product-items-actions">
                                        <div className="add-to-cart actions-primary">
                                            <form action="#" method="post">
                                                <div className="field qty">
                                                    <div className="control qty-change">
                                                        <button className="decreaseqty" id="344-dec">-</button>
                                                        <input readonly="" type="text" name="qty" value="1" title="Qty" className="input-text qty" />
                                                        <button className="increaseqty" id="344-upt">+</button>
                                                    </div>
                                                </div>
                                                <div className="custom-option-container"
                                                ><div className="custom-options">
                                                        <label>Selected Weight </label>
                                                        <select name="options">
                                                            <option value="1">1kg</option>
                                                            <option value="2">2kg</option>
                                                            <option value="3">3kg</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <button type="submit" title="Add to Cart" className="action tocart primary">
                                                    <span>Add to Cart</span>
                                                </button>

                                            </form>
                                        </div>
                                        <div className="actions-secondary wishlist">
                                            <a href="#" className="action towishlist" title="Add to Wish List" role="button">
                                                <span>Add to Wish List</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div className="col-sm-4 col-lg-3 gallery-img-holder">
                            <div className="product-wrapper">

                                <div className="product-wrapper-container">
                                    <a href="#">
                                        <img className="product-img" src={require('../images/english-placeholder_1.png')} alt="" />
                                    </a>

                                    <div className="product-hover">
                                        <ul>
                                            <li className="quickview">
                                                <a rel="nofollow" className="weltpixel-quickview weltpixel_quickview_button_v2" href="javascript:void(0);"></a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div className="product_sticker">
                                        <span className="sticker-label Organic">Organic</span>
                                        <span className="sticker-label Milk Fed">Milk Fed</span>
                                    </div>
                                </div>
                                <div className="product-desc">

                                    <h4 className="product-name">
                                        <a href="#">Beef Breakfast Slices - Chefkom</a>
                                    </h4>
                                    <div className="review-container">
                                        <div className="product-reviews-summary short">
                                            <div className="rating-summary">
                                                <span className="label">
                                                    <span>Rating:</span></span>
                                                <div className="rating-result" title="93%">
                                                    <span style={{ width: "93%" }}><span>93%</span></span>
                                                </div>
                                            </div>
                                            <div className="reviews-actions">
                                                <a className="action view" href="#">4&nbsp;<span>Reviews</span></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="product-price">
                                        <div className="price-box price-final_price">
                                            <span className="price-container price-final_price tax weee">
                                                <span id="product-price-289" className="price-wrapper ">
                                                    <span className="price">KWD 5.700</span>
                                                </span>
                                            </span>
                                            <span className="average-weight">
                                                Average Weight 2.5 to 3.5 Kg
                                            </span>
                                        </div>
                                    </div>
                                    <div className="product-items-actions">
                                        <div className="add-to-cart actions-primary">
                                            <form action="#" method="post">
                                                <div className="field qty">
                                                    <div className="control qty-change">
                                                        <button className="decreaseqty" id="344-dec">-</button>
                                                        <input readonly="" type="text" name="qty" value="1" title="Qty" className="input-text qty" />
                                                        <button className="increaseqty" id="344-upt">+</button>
                                                    </div>
                                                </div>

                                                <button type="submit" title="Add to Cart" className="action tocart primary">
                                                    <span>Add to Cart</span>
                                                </button>

                                            </form>
                                        </div>
                                        <div className="actions-secondary wishlist">
                                            <a href="#" className="action towishlist" title="Add to Wish List" role="button">
                                                <span>Add to Wish List</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div className="col-sm-4 col-lg-3 gallery-img-holder">
                            <div className="product-wrapper">

                                <div className="product-wrapper-container">
                                    <a href="#">
                                        <img className="product-img" src={require('../images/english-placeholder_1.png')} alt="" />
                                    </a>

                                    <div className="product-hover">
                                        <ul>
                                            <li className="quickview">
                                                <a rel="nofollow" className="weltpixel-quickview weltpixel_quickview_button_v2" href="javascript:void(0);"></a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div className="product_sticker">
                                        <span className="sticker-label Organic">Organic</span>
                                        <span className="sticker-label Milk Fed">Milk Fed</span>
                                    </div>
                                </div>
                                <div className="product-desc">

                                    <h4 className="product-name">
                                        <a href="#">Beef Breakfast Slices - Chefkom</a>
                                    </h4>
                                    <div className="review-container">
                                        <div className="product-reviews-summary short">
                                            <div className="rating-summary">
                                                <span className="label">
                                                    <span>Rating:</span></span>
                                                <div className="rating-result" title="93%">
                                                    <span style={{ width: "93%" }}><span>93%</span></span>
                                                </div>
                                            </div>
                                            <div className="reviews-actions">
                                                <a className="action view" href="#">4&nbsp;<span>Reviews</span></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="product-price">
                                        <div className="price-box price-final_price">
                                            <span className="price-container price-final_price tax weee">
                                                <span id="product-price-289" className="price-wrapper ">
                                                    <span className="price">KWD 5.700</span>
                                                </span>
                                            </span>
                                            <span className="average-weight">
                                                Average Weight 2.5 to 3.5 Kg
                                            </span>
                                        </div>
                                    </div>
                                    <div className="product-items-actions">
                                        <div className="add-to-cart actions-primary">
                                            <form action="#" method="post">
                                                <div className="field qty">
                                                    <div className="control qty-change">
                                                        <button className="decreaseqty" id="344-dec">-</button>
                                                        <input readonly="" type="text" name="qty" value="1" title="Qty" className="input-text qty" />
                                                        <button className="increaseqty" id="344-upt">+</button>
                                                    </div>
                                                </div>

                                                <button type="submit" title="Add to Cart" className="action tocart primary">
                                                    <span>Add to Cart</span>
                                                </button>

                                            </form>
                                        </div>
                                        <div className="actions-secondary wishlist">
                                            <a href="#" className="action towishlist" title="Add to Wish List" role="button">
                                                <span>Add to Wish List</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div className="shop-all-button">
                    <a href="#">Shop More</a>
                </div>
            </div>
            <NeedHelp/>
        </React.Fragment>
    );
}

export default Shop;

