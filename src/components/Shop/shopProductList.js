import React from 'react';
import ReactDOM from 'react-dom';
import ShopSortBy from './ShopSortBy';
import ShopGallery from './shopGallery';
const ShopProductList = () => {

    return (
        <React.Fragment>
            <ShopSortBy/>
            <ShopGallery/>
        </React.Fragment>
    );
}

export default ShopProductList;