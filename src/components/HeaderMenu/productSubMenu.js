import React from 'react';

const ProductSubMenu =(props)=> {

    return (
      <React.Fragment>

        <div className="menu-right">
            <ul className="menu-links-right">
                <li className="first-level">
                    <ul>
                        {props.productCategories.length > 0 ? props.productCategories.slice(0, 3).map( ( item, index)=>{
                            return(
                                <li>
                                    <a href="#">{item.name}</a>
                                    <ul className="level3">
                                        {item.children_data && item.children_data.length>0 ? item.children_data.map( ( itemChild, index)=>{
                                        return(
                                            <li className="third-level">
                                            <a href="#">{itemChild.name}</a>
                                            </li>
                                        )
                                    }):''}
                                    </ul>
                               </li>
                            )
                            
                        }) :''}
                    </ul>
                    <ul>
                        {props.productCategories.length > 0 ? props.productCategories.slice(3, 6).map( ( item, index)=>{
                            return(
                                <li>
                                    <a href="#">{item.name}</a>
                                    <ul className="level3">
                                        {item.children_data && item.children_data.length>0 ? item.children_data.map( ( itemChild, index)=>{
                                        return(
                                            <li className="third-level">
                                            <a href="#">{itemChild.name}</a>
                                            </li>
                                        )
                                    }):''}
                                    </ul>
                               </li>
                            )
                            
                        }) :''}
                    </ul>
                </li>
            </ul>
        </div> 
        
      </React.Fragment>
    );
  }
  
  export default ProductSubMenu;
  