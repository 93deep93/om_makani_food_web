import React from 'react';
import ReactDOM from 'react-dom';
import '../../styles/FindDoorstep.scss';

const FindDoorstep = () => {

    return (
        <React.Fragment>
            <section className="find-door-setp-sec">
                <div className="container">
                    <div className="row">
                        <div className="col-md-6">
                            <div className="search-section-explore">
                                <div className="freshest-left-sec">
                                    <h3>Your Favorite Makani Items <br />
                                        Delivered to Your Doorstep</h3>
                                    <div className="block block-search">
                                        <div className="block block-title"><strong>Search and you shall find</strong></div>
                                        <div className="block block-content">
                                            <form className="form minisearch" id="search_mini_form_fresh" action="#" method="get">
                                                <div className="field search">
                                                    <label className="label" htmlFor="search">
                                                        <span>Search</span>
                                                    </label>
                                                    <div className="control">
                                                        <input id="search" type="text" name="q" value="" placeholder="Search" className="input-text" />
                                                        <div id="search_autocomplete" className="search-autocomplete"></div>
                                                    </div>
                                                </div>
                                                <div className="actions">
                                                    <button type="submit" title="Search" className="action search">
                                                        <span>Search</span>
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div className="col-md-6">
                             <div className="pageicons"></div>
                        </div>
                    </div>
                </div>
            </section>


        </React.Fragment>
    );
}

export default FindDoorstep;