import React from 'react';
import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
import '../../styles/Slider.scss';
const options = {
  items: 1,
};
const Banner = () => {

  return (
    <React.Fragment>
      <div className=" top_banner banner-slider">
        <OwlCarousel
          className="owl-theme"
          loop
          margin={10} items={1}
          nav={false}
          autoplay={true}
          autoplayTimeout={5000}
          smartSpeed={500}
        >
          <div className="item">
          <img src={require('../../images/slider-01-opt.webp')} title="Makani Foods" alt="Makani Foods" />
          <div className="banner-slider-content">
              <h1>You choose,<br/>we deliver</h1>
              <p>During this difficult time, we'd like to help make things simpler</p>
              <p>For you and deliver your favorite Makani items safely to your door.</p>
              <div className="banner-slider-btn">
                <a className="banner-btn shop-now" href="#">Shop Now</a></div>
              </div>
          </div>
          <div className="item">
            <div className="bannersec">
          <img src={require('../../images/chicken_wb_1.webp')} title="Makani Foods" alt="Makani Foods" />
          <div className="banner-slider-content light-theme">
              <h1>You choose,<br/>we deliver</h1>
              <p>During this difficult time, we'd like to help make things simpler for you and deliver your favorite Makani items safely to your door.</p>
              
              <div className="banner-slider-btn">
                <a className="banner-btn shop-now" href="#">Shop Now</a></div>
              </div>
            </div>
          </div>
          
          <div className="item">
          <img src={require('../../images/wholefishbanner_1.webp')} title="Makani Foods" alt="Makani Foods" />
          <div className="banner-slider-content light-theme">
              <h1>You choose,<br/>we deliver</h1>
              <p>During this difficult time, we'd like to help make things simpler for you and deliver your favorite Makani items safely to your door.</p>
              
              <div className="banner-slider-btn">
                <a className="banner-btn shop-now" href="#">Shop Now</a></div>
              </div>
          </div>
          <div className="item">
          <img src={require('../../images/salmonbanner_1.webp')} title="Makani Foods" alt="Makani Foods" />
          <div className="banner-slider-content">
              <h1>You choose,<br/>we deliver</h1>
              <p>During this difficult time, we'd like to help make things simpler for you and deliver your favorite Makani items safely to your door.</p>
              
              <div className="banner-slider-btn">
                <a className="banner-btn shop-now" href="#">Shop Now</a></div>
              </div>
          </div>
          
        </OwlCarousel>
      </div>
      <div className="aboutsec">
        <div className="container-fluid">
          <div className="row">
            <div className="col-md-6 pd-0">
            <div className="top-images-content" >
              <h2>Find the perfect sides for your next meal…</h2>
              <p>Check out our creative range: Curly Fries, Sweet Potato Fries and more!</p>
              <p><a href="#">Explore</a></p>
            </div>
            </div>
            <div className="col-md-3 pd-0">
              <div className="makaninext"> </div>
            </div>
            <div className="col-md-3 pd-0">
            <div className="makani-about-next"> </div>
            </div>
          </div>
          <div className="row">
            <div className="col-md-3 pd-0">
            <div className="top-images-content greenbg" >
              <p>For a fast and easy to cook meal, try out our succulent shrimp range! highest in quality and perfect in each bite</p>
              <p><a href="#">Explore</a></p>
            </div>
            </div>
            <div className="col-md-3 pd-0">
              <div className="makaninextthird"> </div>
            </div>
            <div className="col-md-3 pd-0">
            <div className="top-images-content bluebg" >
              <p>Buttery in flavor and pillowy in texture - our Martin’s Potato Rolls are a favorite amongst families, chefs, and foodies!</p>
              <p><a href="#">Explore</a></p>
            </div>
            </div>
            <div className="col-md-3 pd-0" style={{color: "red"}}>
            <div className="makaninextfourth"> </div>
            </div>
          </div>
        </div>
      </div>
      <div className="wedohome">
        <div className="container">
          <div className="row">
              <div className="col-md-12">
                  <h2>We do home delivery</h2>
              </div>
          </div>
          <div className="row">
              <div className="col-md-3 col-sm-6 col-xs-6">
                <div className="makani-icon">
                  <img src={require('../../images/shop.svg')}/>
                  <h4>Shop</h4>
                </div>
              </div>
              <div className="col-md-3 col-sm-6 col-xs-6">
                <div className="makani-icon">
                  <img src={require('../../images/recieve.svg')}/>
                  <h4>Recieve</h4>
                </div>
              </div>
              <div className="col-md-3 col-sm-6 col-xs-6">
                <div className="makani-icon">
                  <img src={require('../../images/cook.svg')}/>
                  <h4>Cook</h4>
                </div>
              </div>
              <div className="col-md-3 col-sm-6 col-xs-6">
                <div className="makani-icon">
                  <img src={require('../../images/enjoy.svg')}/>
                  <h4>Enjoy</h4>
                </div>
              </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
}

export default Banner;