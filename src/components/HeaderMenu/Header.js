import React, {useEffect, useState} from 'react';
import { Link } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import ShopSubMenu from './shopSubMenu';
import '../../styles/Header.scss';
import '../../styles/MediaQuery.scss';
import { getProductsCategories } from '../../Api/commonApi';


const Header = () => {

  const [ categories, setCategories ] = useState([])

  useEffect(() => {
    // code to run on component mount
   
    getProductsCategories( )
      .then(res => {
        const cats = res.hits.hits.reduce((result, cat, index)=>{
          if( cat._source.parent_id )
          result.push(cat._source);
          return result;
        },[]);
        setCategories(cats);
      });
  }, [])

  return (
    <React.Fragment>
      
      <header className="page-header">
        <div className="header content header-container">
          <span data-action="toggle-nav" className="action nav-toggle">
            <span>Toggle Nav</span>
          </span>
          <a className="logo english" href="#" title="Makani Foods" aria-label="store logo">
            <img src={require('../../images/logo-english.svg')} title="Makani Foods" alt="Makani Foods" width="189" height="75" />
          </a>
          <a className="logo arabic" href="#" title="Makani Foods" aria-label="store logo">
            <img src={require('../../images/logo-arabic.svg')} title="Makani Foods" alt="Makani Foods" width="189" height="75" />
          </a>
          <div className="sections nav-sections">
            <div className="nav-sections-item-content">
              <nav className="navigation" data-action="navigation">
                <div id="mySidenav" className="nav">
                  <div className="nav-main">
                    <ul className="">
                      <li className="level0">
                        <Link className="level-top"to="/Shop">Shop</Link>
                        < ShopSubMenu categories={categories} name ={'name'} />
                      </li>
                      <li className="level0">
                        <a className="level-top" href="#">How It works</a>
                      </li>
                      <li className="level0">
                        <a className="level-top" href="#">Showroom Locations</a>
                      </li>
                      <li className="level0">
                        <a className="level-top" href="#">Recipes</a>
                      </li>
                      <li className="level0">
                        <a className="level-top" href="#">How-to</a>
                      </li>
                    </ul>
                  </div>
                </div>
              </nav>
            </div>
          </div>
          <div className="right-container-wrap">
            <ul className="header links">
              <div className="mainmenu">
                <div className="sectionmenu">
                  <div className="secd">
                    <li>
                      <a href="#">Help
                    </a>
                    </li>
                    <li>
                      <a href="#">About Makani
                    </a>
                    </li>
                  </div>
                </div>
              </div>
              <li className="authorization-link">
                <a href="#">
                  <img src={require('../../images/User.svg')} className="user-icon" alt="user-icon" />
                </a>
                <div className="my-account-links">
                  <ul className="login list-item">
                    <li className="authorization-link" >
                      <a href="#">
                        Login
                      </a>
                    </li>
                  </ul>
                  <ul className="profile list-item">
                    <li>
                      <a href="#">My Profile</a>
                    </li>
                  </ul>
                  <ul className="my-order list-item">
                    <li>
                      <a href="#">My Orders</a>
                    </li>
                  </ul>
                  <ul className="support list-item">
                    <li>
                      <a href="#">Support &amp; Contact</a>
                    </li>
                  </ul>
                </div>
              </li>
              <div className="login_overlay_bg"></div>
              <li className="greet welcome">
                <span className="not-logged-in"></span>
              </li>
              <div className="switcher store switcher-store" id="switcher-store">
                <strong className="label switcher-label">
                  <span>Select Store</span>
                </strong>
                <div className="actions dropdown options switcher-options">
                  <li className="switcher-option">
                    <a href="javascript:void(0)">اللغة العربية </a>
                  </li>
                </div>
              </div>
            </ul>
            <div className="minicart-wrapper">
              <a className="action showcart" href="#">
                <span className="text">My Cart</span>
                <span className="counter qty empty">
                  <span className="counter-number">

                  </span>
                  <span className="counter-label">

                  </span>
                </span>
              </a>
              <div className="ui-dialog ui-widget ui-widget-content ui-corner-all ui-front mage-dropdown-dialog" tabIndex="-1" role="dialog">
                <div className="block block-minicart ui-dialog-content ui-widget-content" id="ui-id-1">
                  <div id="minicart-content-wrapper">

                    <div className="block-title">
                      <strong>
                        <span className="text">My Cart</span>
                        <span className="qty empty" title="Items in Cart">0</span>
                      </strong>
                    </div>
                    <div className="block-content">
                      <button type="button" id="btn-minicart-close" className="action close" title="Close">
                        <span data-bind="i18n: 'Close'">Close</span>
                      </button>

                      <strong className="subtitle empty">You have no items in your shopping cart.</strong>

                    </div>

                  </div>
                </div>
              </div>
            </div>
            <div className="block block-search">
              <div className="block block-title">
                <strong>Search and you shall find</strong>
              </div>
              <div className="block block-content">
                <form className="form minisearch" id="search_mini_form" action="#" method="get">
                  <div className="field search">
                    <label className="label" htmlFor="search" data-role="minisearch-label">
                      <span>Search</span>
                    </label>
                    <div className="control">
                      <input id="search" type="text" name="q" value="" placeholder="Search" className="input-text" maxLength="128" role="combobox" />
                      <div id="search_autocomplete" className="search-autocomplete"></div>
                    </div>
                  </div>
                  <div className="actions">
                    <button type="submit" title="Search" className="action search" aria-label="Search" disabled="">
                      <span>Search</span>
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
          <div className="login-overlay">
            <div className="login-popup">
              <div className="response-msg"></div>
              <div className="login-container">
                <a href="#">Create account</a>
                <p>Welcome Back :)</p>
                <form className="popup-login-form" method="POST" action="">
                  <fieldset className="fieldset">
                    <div className="container field required">
                      <input type="text" placeholder="E-mail or phone no." name="username" autoComplete="off" required="" />
                      <input type="password" placeholder="Password" name="password" autoComplete="off" required="" />
                      <div className="forgot">
                        <a href="#">Forgot Your password ?</a>
                      </div>
                      <button className="action login primary" type="submit">Sign In</button>
                    </div>
                  </fieldset>
                </form>
              </div>
            </div>
          </div>
        </div>
      </header>
     
      <div className="delivery-container">
        <div className="container-fluid">
          <div className="row">
            <div className="col-md-12">
              <div className="free-delivery-text-main">
                <div className="free-delivery-textmain">
                  <p className="free-delivery-text">The minimum order for delivery is KD 10 - all displayed products are frozen for maximized freshness
                      <img src={require('../../images/frozen.mst.webp')} alt="frozen" />
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
}


export default Header;