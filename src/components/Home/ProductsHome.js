import React from 'react';
import ReactDOM from 'react-dom';
import "../../styles/ProductHome.scss";

const ProductsHome = () => {

    return (
        <React.Fragment>
            <div className="product-display">
                <div className="container">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="product-display-head">
                                <h3>Top Products</h3>
                                <p className="product-sub-head">Most popular products this week</p>
                            </div>
                        </div>
                    </div>

                    <div className="row mtb-20">
                        <div className="col-md-12">
                            <ul className="nav nav-pills gallery-filters">
                                <li role="presentation" className="active" >
                                    <a href="#">All</a>
                                </li>
                                <li role="presentation">
                                    <a href="#">Poultry</a>
                                </li>
                                <li role="presentation">
                                    <a href="#">Beef</a>
                                </li>
                                <li role="presentation">
                                    <a href="#">Organic</a>
                                </li>
                                <li role="presentation">
                                    <a href="#">Seafood</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div className="row gallery-images">
                        <div className="col-sm-3 gallery-img-holder">
                            <div className="product-wrapper">

                                <div className="product-wrapper-container">
                                    <a href="#">
                                        <img className="product-img" src={require('../../images/english-placeholder_1.png')} alt="" />
                                    </a>
                                    <div className="product-hover">
                                        <ul>
                                            <li className="wishlist">
                                                <a href="#" className="action towishlist">
                                                </a>
                                            </li>
                                            <li className="quickview">
                                                <a rel="nofollow" className="weltpixel-quickview weltpixel_quickview_button_v2" href="javascript:void(0);"></a>
                                            </li>
                                            <li className="add-to-cart">
                                                <div className="actions add-to-cart">
                                                    <button type="submit" title="Add to Bag" className="action tocart primary">
                                                        <span>Add to Bag</span>
                                                    </button>
                                                    <div className="product-item-inner">
                                                        <button type="submit" title="Add to Bag" className="action tocart primary"></button>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>

                                    </div>

                                </div>
                                <div className="product-desc">
                                    <h4 className="product-name">
                                        <a href="#">Beef Breakfast Slices - Chefkom</a>
                                    </h4>
                                    <div className="product-price">
                                        <div className="price-box price-final_price">
                                            <span className="price-container price-final_price tax weee">
                                                <span id="product-price-289" className="price-wrapper ">
                                                    <span className="price">KWD 5.700</span>
                                                </span>
                                            </span>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div className="col-sm-3 gallery-img-holder">
                            <div className="product-wrapper">

                                <div className="product-wrapper-container">
                                    <a href="#">
                                        <img className="product-img" src={require('../../images/english-placeholder_1.png')} alt="" />
                                    </a>
                                    <div className="product-hover">
                                        <ul>
                                            <li className="wishlist">
                                                <a href="#" className="action towishlist">
                                                </a>
                                            </li>
                                            <li className="quickview">
                                                <a rel="nofollow" className="weltpixel-quickview weltpixel_quickview_button_v2" href="javascript:void(0);"></a>
                                            </li>
                                            <li className="add-to-cart">
                                                <div className="actions add-to-cart">
                                                    <button type="submit" title="Add to Bag" className="action tocart primary">
                                                        <span>Add to Bag</span>
                                                    </button>
                                                    <div className="product-item-inner">
                                                        <button type="submit" title="Add to Bag" className="action tocart primary"></button>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>

                                    </div>

                                </div>
                                <div className="product-desc">
                                    <h4 className="product-name">
                                        <a href="#">Beef Breakfast Slices - Chefkom</a>
                                    </h4>
                                    <div className="product-price">
                                        <div className="price-box price-final_price">
                                            <span className="price-container price-final_price tax weee">
                                                <span id="product-price-289" className="price-wrapper ">
                                                    <span className="price">KWD 5.700</span>
                                                </span>
                                            </span>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div className="col-sm-3 gallery-img-holder">
                            <div className="product-wrapper">

                                <div className="product-wrapper-container">
                                    <a href="#">
                                        <img className="product-img" src={require('../../images/english-placeholder_1.png')} alt="" />
                                    </a>
                                    <div className="product-hover">
                                        <ul>
                                            <li className="wishlist">
                                                <a href="#" className="action towishlist">
                                                </a>
                                            </li>
                                            <li className="quickview">
                                                <a rel="nofollow" className="weltpixel-quickview weltpixel_quickview_button_v2" href="javascript:void(0);"></a>
                                            </li>
                                            <li className="add-to-cart">
                                                <div className="actions add-to-cart">
                                                    <button type="submit" title="Add to Bag" className="action tocart primary">
                                                        <span>Add to Bag</span>
                                                    </button>
                                                    <div className="product-item-inner">
                                                        <button type="submit" title="Add to Bag" className="action tocart primary"></button>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>

                                    </div>

                                </div>
                                <div className="product-desc">
                                    <h4 className="product-name">
                                        <a href="#">Beef Breakfast Slices - Chefkom</a>
                                    </h4>
                                    <div className="product-price">
                                        <div className="price-box price-final_price">
                                            <span className="price-container price-final_price tax weee">
                                                <span id="product-price-289" className="price-wrapper ">
                                                    <span className="price">KWD 5.700</span>
                                                </span>
                                            </span>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div className="col-sm-3 gallery-img-holder">
                            <div className="product-wrapper">

                                <div className="product-wrapper-container">
                                    <a href="#">
                                        <img className="product-img" src="https://dev.makanifoods.com/media/catalog/product/cache/02db20edfbe38a05819d9dfb7c772bfa/b/e/beef_breakfast_slices_1_1.jpg" alt="" />
                                    </a>
                                    <div className="product-hover">
                                        <ul>
                                            <li className="wishlist">
                                                <a href="#" className="action towishlist">
                                                </a>
                                            </li>
                                            <li className="quickview">
                                                <a rel="nofollow" className="weltpixel-quickview weltpixel_quickview_button_v2" href="javascript:void(0);"></a>
                                            </li>
                                            <li className="add-to-cart">
                                                <div className="actions add-to-cart">
                                                    <button type="submit" title="Add to Bag" className="action tocart primary">
                                                        <span>Add to Bag</span>
                                                    </button>
                                                    <div className="product-item-inner">
                                                        <button type="submit" title="Add to Bag" className="action tocart primary"></button>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>

                                    </div>

                                </div>
                                <div className="product-desc">
                                    <h4 className="product-name">
                                        <a href="#">Beef Breakfast Slices - Chefkom</a>
                                    </h4>
                                    <div className="product-price">
                                        <div className="price-box price-final_price">
                                            <span className="price-container price-final_price tax weee">
                                                <span id="product-price-289" className="price-wrapper ">
                                                    <span className="price">KWD 5.700</span>
                                                </span>
                                            </span>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>
                <div className="shop-all-button">
                    <a href="#">Shop More</a>
                </div>
            </div>
            <section className="shopby">
                <div className="container">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="shopbyhead">
                                <h3>Shop by : We do home Delivery</h3>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-12">
                            <ul className="pick-cart-items">
                                <li><a href="#">
                                <div className="pick-cut-img">
                                    <img className="shrimp" src={require('../../images/shrimp_1.png')} alt="Shrimp" /></div>
                                <div className="title-name">
                                    <span>Shop Shrimp</span>
                                </div>
                            </a>
                            </li>
                                <li><a href="#">
                                    <div className="pick-cut-img">
                                        <img className="Poultry" src={require('../../images/poultry-he.png')} alt="Poultry" /></div>
                                    <div className="title-name">
                                        <span>Shop Poultry</span>
                                    </div>
                                </a>
                                </li>
                                <li><a href="#">
                                    <div className="pick-cut-img">
                                        <img className="steaks" src={require('../../images/steak-new.png')}  alt="Steaks" /></div>
                                    <div className="title-name">
                                        <span>Shop Steaks</span>
                                    </div>
                                </a>
                                </li>
                                <li><a href="#">
                                    <div className="pick-cut-img">
                                        <img className="whole-fish" src={require('../../images/whole-fish-new.png')}  alt="Whole Fish" /></div>
                                    <div className="title-name">
                                        <span>Shop Whole Fish</span>
                                    </div>
                                </a>
                                </li>
                                <li><a href="#">
                                    <div className="pick-cut-img">
                                        <img className="fries-potato" src={require('../../images/fries-new.png')}  alt="Fries &amp; Potato" /></div>
                                    <div className="title-name">
                                        <span>Shop Fries &amp; Potato</span>
                                    </div>
                                </a>
                                </li>
                                <li><a href="#">
                                    <div className="pick-cut-img">
                                        <img className="cheescakes" src="https://dev.makanifoods.com/media/wysiwyg/homepage/cheesecake.png" alt="Cheescakes" /></div>
                                    <div className="title-name">
                                        <span>Shop Cheescakes</span>
                                    </div>
                                </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>
        </React.Fragment>
    );
}

export default ProductsHome;

